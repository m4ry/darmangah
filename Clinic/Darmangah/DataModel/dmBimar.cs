﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Darmangah.DataModel
{
    class dmBimar
    {
        public int id { get; set; }
        public string name { get; set; }
        public string codeMeli { get; set; }
        public string Bime { get; set; }
        public string kodeBime { get; set; }
        public string ChooseKhedmat { get; set; }
        public long ChooseDoc { get; set; }
        public long DatePaziresh { get; set; }
        public string ShomarePaziresh { get; set; }
        public long Mobile { get; set; }
        public string Mablagh { get; set; }
        public string Pardakhti { get; set; }
        public string MandePardakhti { get; set; }
    }
}
