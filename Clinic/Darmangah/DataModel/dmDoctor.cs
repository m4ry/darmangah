﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Darmangah.DataModel
{
    class dmDoctor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeMeli { get; set; }
        public string Gender { get; set; }
        public long Tel { get; set; }
        public long Mobile { get; set; }
        public long Address { get; set; }
        public long Nezam { get; set; }
        public long NoeTakhassos { get; set; }
        public long Tahsilat { get; set; }
        public long Shift { get; set; }
        public long DateHozoor { get; set; }

    }
}
