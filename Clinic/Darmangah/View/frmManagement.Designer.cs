﻿namespace Darmangah.View
{
    partial class frmManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabfrmManage_Choose = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabfrmManage_Choose.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabfrmManage_Choose
            // 
            this.tabfrmManage_Choose.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.tabfrmManage_Choose.Controls.Add(this.tabPage1);
            this.tabfrmManage_Choose.Controls.Add(this.tabPage2);
            this.tabfrmManage_Choose.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabfrmManage_Choose.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabfrmManage_Choose.ItemSize = new System.Drawing.Size(25, 100);
            this.tabfrmManage_Choose.Location = new System.Drawing.Point(40, 0);
            this.tabfrmManage_Choose.Multiline = true;
            this.tabfrmManage_Choose.Name = "tabfrmManage_Choose";
            this.tabfrmManage_Choose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabfrmManage_Choose.SelectedIndex = 0;
            this.tabfrmManage_Choose.Size = new System.Drawing.Size(744, 561);
            this.tabfrmManage_Choose.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabfrmManage_Choose.TabIndex = 0;
            this.tabfrmManage_Choose.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabfrmManage_Choose_DrawItem);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(636, 553);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "پزشک";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(636, 553);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "بیمار";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // frmManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tabfrmManage_Choose);
            this.Name = "frmManagement";
            this.Text = "مدیریت خدمات";
            this.tabfrmManage_Choose.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabfrmManage_Choose;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}