﻿namespace Darmangah
{
    partial class frmChoose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChoose));
            this.btnfrmChoose_Nobatdehi = new System.Windows.Forms.Button();
            this.btnfrmChoose_ManageDoc = new System.Windows.Forms.Button();
            this.btnfrmChoose_Manage = new System.Windows.Forms.Button();
            this.btnfrmChoose_Report = new System.Windows.Forms.Button();
            this.btnfrmChoose_ChangePass = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnfrmChoose_Nobatdehi
            // 
            this.btnfrmChoose_Nobatdehi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnfrmChoose_Nobatdehi.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmChoose_Nobatdehi.Image = global::Darmangah.ImageResource.btnfrmChoose_Nobatdehi;
            this.btnfrmChoose_Nobatdehi.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmChoose_Nobatdehi.Location = new System.Drawing.Point(133, 27);
            this.btnfrmChoose_Nobatdehi.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnfrmChoose_Nobatdehi.Name = "btnfrmChoose_Nobatdehi";
            this.btnfrmChoose_Nobatdehi.Size = new System.Drawing.Size(166, 70);
            this.btnfrmChoose_Nobatdehi.TabIndex = 0;
            this.btnfrmChoose_Nobatdehi.Text = "سیستم نوبت دهی   ";
            this.btnfrmChoose_Nobatdehi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmChoose_Nobatdehi.UseVisualStyleBackColor = true;
            this.btnfrmChoose_Nobatdehi.Click += new System.EventHandler(this.btnfrmChoose_Nobatdehi_Click);
            this.btnfrmChoose_Nobatdehi.MouseLeave += new System.EventHandler(this.btnfrmChoose_Nobatdehi_MouseLeave);
            this.btnfrmChoose_Nobatdehi.MouseHover += new System.EventHandler(this.btnfrmChoose_Nobatdehi_MouseHover);
            // 
            // btnfrmChoose_ManageDoc
            // 
            this.btnfrmChoose_ManageDoc.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmChoose_ManageDoc.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmChoose_ManageDoc.Image")));
            this.btnfrmChoose_ManageDoc.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmChoose_ManageDoc.Location = new System.Drawing.Point(133, 104);
            this.btnfrmChoose_ManageDoc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnfrmChoose_ManageDoc.Name = "btnfrmChoose_ManageDoc";
            this.btnfrmChoose_ManageDoc.Size = new System.Drawing.Size(166, 70);
            this.btnfrmChoose_ManageDoc.TabIndex = 1;
            this.btnfrmChoose_ManageDoc.Text = "مدیریت پزشک";
            this.btnfrmChoose_ManageDoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmChoose_ManageDoc.UseVisualStyleBackColor = true;
            this.btnfrmChoose_ManageDoc.MouseLeave += new System.EventHandler(this.btnfrmChoose_ManageDoc_MouseLeave);
            this.btnfrmChoose_ManageDoc.MouseHover += new System.EventHandler(this.btnfrmChoose_ManageDoc_MouseHover);
            // 
            // btnfrmChoose_Manage
            // 
            this.btnfrmChoose_Manage.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmChoose_Manage.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmChoose_Manage.Image")));
            this.btnfrmChoose_Manage.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmChoose_Manage.Location = new System.Drawing.Point(133, 255);
            this.btnfrmChoose_Manage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnfrmChoose_Manage.Name = "btnfrmChoose_Manage";
            this.btnfrmChoose_Manage.Size = new System.Drawing.Size(166, 70);
            this.btnfrmChoose_Manage.TabIndex = 2;
            this.btnfrmChoose_Manage.Text = "مدیریت خدمات";
            this.btnfrmChoose_Manage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmChoose_Manage.UseVisualStyleBackColor = true;
            this.btnfrmChoose_Manage.MouseLeave += new System.EventHandler(this.btnfrmChoose_Manage_MouseLeave);
            this.btnfrmChoose_Manage.MouseHover += new System.EventHandler(this.btnfrmChoose_Manage_MouseHover);
            // 
            // btnfrmChoose_Report
            // 
            this.btnfrmChoose_Report.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnfrmChoose_Report.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmChoose_Report.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmChoose_Report.Image")));
            this.btnfrmChoose_Report.Location = new System.Drawing.Point(133, 180);
            this.btnfrmChoose_Report.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnfrmChoose_Report.Name = "btnfrmChoose_Report";
            this.btnfrmChoose_Report.Size = new System.Drawing.Size(166, 70);
            this.btnfrmChoose_Report.TabIndex = 3;
            this.btnfrmChoose_Report.Text = "گزارش گیری";
            this.btnfrmChoose_Report.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmChoose_Report.UseVisualStyleBackColor = true;
            // 
            // btnfrmChoose_ChangePass
            // 
            this.btnfrmChoose_ChangePass.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmChoose_ChangePass.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmChoose_ChangePass.Image")));
            this.btnfrmChoose_ChangePass.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmChoose_ChangePass.Location = new System.Drawing.Point(133, 332);
            this.btnfrmChoose_ChangePass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnfrmChoose_ChangePass.Name = "btnfrmChoose_ChangePass";
            this.btnfrmChoose_ChangePass.Size = new System.Drawing.Size(166, 70);
            this.btnfrmChoose_ChangePass.TabIndex = 4;
            this.btnfrmChoose_ChangePass.Text = "تغییر رمز عبور";
            this.btnfrmChoose_ChangePass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmChoose_ChangePass.UseVisualStyleBackColor = true;
            this.btnfrmChoose_ChangePass.MouseLeave += new System.EventHandler(this.btnfrmChoose_ChangePass_MouseLeave);
            this.btnfrmChoose_ChangePass.MouseHover += new System.EventHandler(this.btnfrmChoose_ChangePass_MouseHover);
            // 
            // frmChoose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnfrmChoose_ChangePass);
            this.Controls.Add(this.btnfrmChoose_Report);
            this.Controls.Add(this.btnfrmChoose_Manage);
            this.Controls.Add(this.btnfrmChoose_ManageDoc);
            this.Controls.Add(this.btnfrmChoose_Nobatdehi);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmChoose";
            this.Text = "انتخاب عملیات";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnfrmChoose_Nobatdehi;
        private System.Windows.Forms.Button btnfrmChoose_ManageDoc;
        private System.Windows.Forms.Button btnfrmChoose_Manage;
        private System.Windows.Forms.Button btnfrmChoose_Report;
        private System.Windows.Forms.Button btnfrmChoose_ChangePass;
    }
}