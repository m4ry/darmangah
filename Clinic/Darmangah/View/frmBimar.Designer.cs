﻿namespace Darmangah.View
{
    partial class frmBimar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBimar));
            this.tabfrmBimar_Nobat = new System.Windows.Forms.TabControl();
            this.tbBimar_NobatDoc = new System.Windows.Forms.TabPage();
            this.txtlblBimar_NobatDoc_KodeBime = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_Mobile = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_Mobile = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_KodeBime = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbBimar_NobatDoc_ChooseKhedmat = new System.Windows.Forms.ComboBox();
            this.txtlblBimar_NobatDoc_ChooseKhedmat = new System.Windows.Forms.Label();
            this.btnBimar_NobatDoc_Print = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_KodeMelli = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_KodeMelli = new System.Windows.Forms.Label();
            this.btnBimar_NobatDoc_Save = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_Bime = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_Bime = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_Name = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_Name = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_MandePardakhti = new System.Windows.Forms.TextBox();
            this.txtBimar_NobatDoc_Pardakhti = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_ShomarePaziresh = new System.Windows.Forms.Label();
            this.txtBimar_NobatDoc_ShomarePaziresh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_Pardakhti = new System.Windows.Forms.Label();
            this.dateBimar_NobatDoc_DatePaziresh = new System.Windows.Forms.DateTimePicker();
            this.txtBimar_NobatDoc_Mablagh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatDoc_MandePardakhti = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatDoc_DatePaziresh = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatDoc_ChooseDoc = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatDoc_Mablagh = new System.Windows.Forms.Label();
            this.cmbBimar_NobatDoc_ChooseDoc = new System.Windows.Forms.ComboBox();
            this.tbBimar_NobatKhadamat = new System.Windows.Forms.TabPage();
            this.txtlblBimar_NobatKhadamat_KodeBime = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_Mobile = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_Mobile = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_KodeBime = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbBimar_NobatKhadamat_ChooseKhedmat = new System.Windows.Forms.ComboBox();
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat = new System.Windows.Forms.Label();
            this.btnBimar_NobatKhadamat_Print = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_Kodemelli = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_Kodemelli = new System.Windows.Forms.Label();
            this.btnBimar_NobatKhadamat_Save = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_Bime = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_Bime = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_Name = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_Name = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_MandePardakhti = new System.Windows.Forms.TextBox();
            this.txtBimar_NobatKhadamat_Pardakhti = new System.Windows.Forms.TextBox();
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh = new System.Windows.Forms.Label();
            this.txtBimar_NobatKhadamat_ShomarePaziresh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_Pardakhti = new System.Windows.Forms.Label();
            this.dateBimar_NobatKhadamat_DatePaziresh = new System.Windows.Forms.DateTimePicker();
            this.txtBimar_NobatKhadamat_Mablagh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatKhadamat_MandePardakhti = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatKhadamat_DatePaziresh = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatKhadamat_ChooseDoc = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatKhadamat_Mablagh = new System.Windows.Forms.Label();
            this.cmbBimar_NobatKhadamat_ChooseDoc = new System.Windows.Forms.ComboBox();
            this.tbBimar_NobatScan = new System.Windows.Forms.TabPage();
            this.txtlblBimar_NobatScan_KodeBime = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_Mobile = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_Mobile = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_KodeBime = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbBimar_NobatScan_ChooseKhedmat = new System.Windows.Forms.ComboBox();
            this.txtlblBimar_NobatScan_ChooseKhedmat = new System.Windows.Forms.Label();
            this.btnBimar_NobatScan_Print = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_Kodemelli = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_Kodemelli = new System.Windows.Forms.Label();
            this.btnBimar_NobatScan_Save = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_Bime = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_Bime = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_Name = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_Name = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_MandePardakhti = new System.Windows.Forms.TextBox();
            this.txtBimar_NobatScan_Pardakhti = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_ShomarePaziresh = new System.Windows.Forms.Label();
            this.txtBimar_NobatScan_ShomarePaziresh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_Pardakhti = new System.Windows.Forms.Label();
            this.dateBimar_NobatScan_DatePaziresh = new System.Windows.Forms.DateTimePicker();
            this.txtBimar_NobatScan_Mablagh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatScan_MandePardakhti = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatScan_DatePaziresh = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatScan_ChooseDoc = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatScan_Mablagh = new System.Windows.Forms.Label();
            this.cmbBimar_NobatScan_ChooseDoc = new System.Windows.Forms.ComboBox();
            this.tbBimar_NobatLab = new System.Windows.Forms.TabPage();
            this.txtlblBimar_NobatLab_KodeBime = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_Mobile = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_Mobile = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_KodeBime = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbBimar_NobatLab_ChooseKhedmat = new System.Windows.Forms.ComboBox();
            this.txtlblBimar_NobatLab_ChooseKhedmat = new System.Windows.Forms.Label();
            this.btnBimar_NobatLab_Print = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_Kodemelli = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_Kodemelli = new System.Windows.Forms.Label();
            this.btnBimar_NobatLab_Save = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_Bime = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_Bime = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_Name = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_Name = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_MandePardakhti = new System.Windows.Forms.TextBox();
            this.txtBimar_NobatLab_Pardakhti = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_ShomarePaziresh = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_ShomarePaziresh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_Pardakhti = new System.Windows.Forms.Label();
            this.dateBimar_NobatLab_DatePaziresh = new System.Windows.Forms.DateTimePicker();
            this.txtBimar_NobatLab_Mablagh = new System.Windows.Forms.TextBox();
            this.txtlblBimar_NobatLab_MandePardakhti = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatLab_DatePaziresh = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatLab_ChooseDoc = new System.Windows.Forms.Label();
            this.txtlblBimar_NobatLab_Mablagh = new System.Windows.Forms.Label();
            this.txtBimar_NobatLab_ChooseDoc = new System.Windows.Forms.ComboBox();
            this.ImgTabBimar = new System.Windows.Forms.ImageList(this.components);
            this.dgvBimar = new System.Windows.Forms.DataGridView();
            this.dbsBimar = new System.Windows.Forms.BindingSource(this.components);
            this.btnfrmBimar_Print = new System.Windows.Forms.Button();
            this.btnfrmBimar_CancleNobat = new System.Windows.Forms.Button();
            this.btnfrmBimar_EditNobat = new System.Windows.Forms.Button();
            this.btnfrmBimar_Search_SearchNobat = new System.Windows.Forms.Button();
            this.txtfrmBimar_Search_Kodemelli = new System.Windows.Forms.TextBox();
            this.datefrmBimar_Search_From = new System.Windows.Forms.DateTimePicker();
            this.datefrmBimar_Search_To = new System.Windows.Forms.DateTimePicker();
            this.grpfrmBimar_Search = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtlblBimar_Search_Kodemelli = new System.Windows.Forms.Label();
            this.codeMeliDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chooseDocDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePazireshDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShomarePaziresh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mablagh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pardakhti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MandePardakhti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabfrmBimar_Nobat.SuspendLayout();
            this.tbBimar_NobatDoc.SuspendLayout();
            this.tbBimar_NobatKhadamat.SuspendLayout();
            this.tbBimar_NobatScan.SuspendLayout();
            this.tbBimar_NobatLab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBimar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsBimar)).BeginInit();
            this.grpfrmBimar_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabfrmBimar_Nobat
            // 
            this.tabfrmBimar_Nobat.Controls.Add(this.tbBimar_NobatDoc);
            this.tabfrmBimar_Nobat.Controls.Add(this.tbBimar_NobatKhadamat);
            this.tabfrmBimar_Nobat.Controls.Add(this.tbBimar_NobatScan);
            this.tabfrmBimar_Nobat.Controls.Add(this.tbBimar_NobatLab);
            this.tabfrmBimar_Nobat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabfrmBimar_Nobat.ImageList = this.ImgTabBimar;
            this.tabfrmBimar_Nobat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabfrmBimar_Nobat.ItemSize = new System.Drawing.Size(190, 20);
            this.tabfrmBimar_Nobat.Location = new System.Drawing.Point(8, 12);
            this.tabfrmBimar_Nobat.Name = "tabfrmBimar_Nobat";
            this.tabfrmBimar_Nobat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabfrmBimar_Nobat.RightToLeftLayout = true;
            this.tabfrmBimar_Nobat.SelectedIndex = 0;
            this.tabfrmBimar_Nobat.Size = new System.Drawing.Size(764, 225);
            this.tabfrmBimar_Nobat.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabfrmBimar_Nobat.TabIndex = 0;
            // 
            // tbBimar_NobatDoc
            // 
            this.tbBimar_NobatDoc.BackColor = System.Drawing.Color.Transparent;
            this.tbBimar_NobatDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_KodeBime);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_Mobile);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_Mobile);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_KodeBime);
            this.tbBimar_NobatDoc.Controls.Add(this.label7);
            this.tbBimar_NobatDoc.Controls.Add(this.cmbBimar_NobatDoc_ChooseKhedmat);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_ChooseKhedmat);
            this.tbBimar_NobatDoc.Controls.Add(this.btnBimar_NobatDoc_Print);
            this.tbBimar_NobatDoc.Controls.Add(this.label6);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_KodeMelli);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_KodeMelli);
            this.tbBimar_NobatDoc.Controls.Add(this.btnBimar_NobatDoc_Save);
            this.tbBimar_NobatDoc.Controls.Add(this.label5);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_Bime);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_Bime);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_Name);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_Name);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_MandePardakhti);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_Pardakhti);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_ShomarePaziresh);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_ShomarePaziresh);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_Pardakhti);
            this.tbBimar_NobatDoc.Controls.Add(this.dateBimar_NobatDoc_DatePaziresh);
            this.tbBimar_NobatDoc.Controls.Add(this.txtBimar_NobatDoc_Mablagh);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_MandePardakhti);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_DatePaziresh);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_ChooseDoc);
            this.tbBimar_NobatDoc.Controls.Add(this.txtlblBimar_NobatDoc_Mablagh);
            this.tbBimar_NobatDoc.Controls.Add(this.cmbBimar_NobatDoc_ChooseDoc);
            this.tbBimar_NobatDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tbBimar_NobatDoc.ImageIndex = 0;
            this.tbBimar_NobatDoc.Location = new System.Drawing.Point(4, 24);
            this.tbBimar_NobatDoc.Name = "tbBimar_NobatDoc";
            this.tbBimar_NobatDoc.Padding = new System.Windows.Forms.Padding(3);
            this.tbBimar_NobatDoc.Size = new System.Drawing.Size(756, 197);
            this.tbBimar_NobatDoc.TabIndex = 0;
            this.tbBimar_NobatDoc.Text = "نوبت پزشک";
            this.tbBimar_NobatDoc.ToolTipText = "dgdfg";
            // 
            // txtlblBimar_NobatDoc_KodeBime
            // 
            this.txtlblBimar_NobatDoc_KodeBime.AutoSize = true;
            this.txtlblBimar_NobatDoc_KodeBime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_KodeBime.Location = new System.Drawing.Point(677, 146);
            this.txtlblBimar_NobatDoc_KodeBime.Name = "txtlblBimar_NobatDoc_KodeBime";
            this.txtlblBimar_NobatDoc_KodeBime.Size = new System.Drawing.Size(65, 17);
            this.txtlblBimar_NobatDoc_KodeBime.TabIndex = 46;
            this.txtlblBimar_NobatDoc_KodeBime.Text = "شماره دفترچه :";
            // 
            // txtBimar_NobatDoc_Mobile
            // 
            this.txtBimar_NobatDoc_Mobile.Location = new System.Drawing.Point(265, 166);
            this.txtBimar_NobatDoc_Mobile.Name = "txtBimar_NobatDoc_Mobile";
            this.txtBimar_NobatDoc_Mobile.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatDoc_Mobile.TabIndex = 44;
            // 
            // txtlblBimar_NobatDoc_Mobile
            // 
            this.txtlblBimar_NobatDoc_Mobile.AutoSize = true;
            this.txtlblBimar_NobatDoc_Mobile.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_Mobile.Location = new System.Drawing.Point(430, 146);
            this.txtlblBimar_NobatDoc_Mobile.Name = "txtlblBimar_NobatDoc_Mobile";
            this.txtlblBimar_NobatDoc_Mobile.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatDoc_Mobile.TabIndex = 43;
            this.txtlblBimar_NobatDoc_Mobile.Text = "تلفن همراه :";
            // 
            // txtBimar_NobatDoc_KodeBime
            // 
            this.txtBimar_NobatDoc_KodeBime.Location = new System.Drawing.Point(521, 168);
            this.txtBimar_NobatDoc_KodeBime.Name = "txtBimar_NobatDoc_KodeBime";
            this.txtBimar_NobatDoc_KodeBime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatDoc_KodeBime.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 17);
            this.label7.TabIndex = 42;
            this.label7.Text = "تومان";
            // 
            // cmbBimar_NobatDoc_ChooseKhedmat
            // 
            this.cmbBimar_NobatDoc_ChooseKhedmat.FormattingEnabled = true;
            this.cmbBimar_NobatDoc_ChooseKhedmat.Items.AddRange(new object[] {
            "پزشک عمومی",
            "متخصص",
            "مامایی",
            "رژیم درمانی",
            "دندانپزشک"});
            this.cmbBimar_NobatDoc_ChooseKhedmat.Location = new System.Drawing.Point(264, 27);
            this.cmbBimar_NobatDoc_ChooseKhedmat.Name = "cmbBimar_NobatDoc_ChooseKhedmat";
            this.cmbBimar_NobatDoc_ChooseKhedmat.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatDoc_ChooseKhedmat.TabIndex = 17;
            // 
            // txtlblBimar_NobatDoc_ChooseKhedmat
            // 
            this.txtlblBimar_NobatDoc_ChooseKhedmat.AutoSize = true;
            this.txtlblBimar_NobatDoc_ChooseKhedmat.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_ChooseKhedmat.Location = new System.Drawing.Point(430, 9);
            this.txtlblBimar_NobatDoc_ChooseKhedmat.Name = "txtlblBimar_NobatDoc_ChooseKhedmat";
            this.txtlblBimar_NobatDoc_ChooseKhedmat.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatDoc_ChooseKhedmat.TabIndex = 6;
            this.txtlblBimar_NobatDoc_ChooseKhedmat.Text = "نوع خدمت :";
            // 
            // btnBimar_NobatDoc_Print
            // 
            this.btnBimar_NobatDoc_Print.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatDoc_Print.Image = ((System.Drawing.Image)(resources.GetObject("btnBimar_NobatDoc_Print.Image")));
            this.btnBimar_NobatDoc_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBimar_NobatDoc_Print.Location = new System.Drawing.Point(11, 161);
            this.btnBimar_NobatDoc_Print.Name = "btnBimar_NobatDoc_Print";
            this.btnBimar_NobatDoc_Print.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatDoc_Print.TabIndex = 39;
            this.btnBimar_NobatDoc_Print.Text = "پرینت نوبت";
            this.btnBimar_NobatDoc_Print.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBimar_NobatDoc_Print.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "تومان";
            // 
            // txtBimar_NobatDoc_KodeMelli
            // 
            this.txtBimar_NobatDoc_KodeMelli.Location = new System.Drawing.Point(522, 73);
            this.txtBimar_NobatDoc_KodeMelli.Name = "txtBimar_NobatDoc_KodeMelli";
            this.txtBimar_NobatDoc_KodeMelli.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatDoc_KodeMelli.TabIndex = 5;
            // 
            // txtlblBimar_NobatDoc_KodeMelli
            // 
            this.txtlblBimar_NobatDoc_KodeMelli.AutoSize = true;
            this.txtlblBimar_NobatDoc_KodeMelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_KodeMelli.Location = new System.Drawing.Point(699, 53);
            this.txtlblBimar_NobatDoc_KodeMelli.Name = "txtlblBimar_NobatDoc_KodeMelli";
            this.txtlblBimar_NobatDoc_KodeMelli.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatDoc_KodeMelli.TabIndex = 4;
            this.txtlblBimar_NobatDoc_KodeMelli.Text = "  کد ملی :";
            // 
            // btnBimar_NobatDoc_Save
            // 
            this.btnBimar_NobatDoc_Save.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatDoc_Save.Location = new System.Drawing.Point(133, 161);
            this.btnBimar_NobatDoc_Save.Name = "btnBimar_NobatDoc_Save";
            this.btnBimar_NobatDoc_Save.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatDoc_Save.TabIndex = 34;
            this.btnBimar_NobatDoc_Save.Text = "ثبت";
            this.btnBimar_NobatDoc_Save.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 17);
            this.label5.TabIndex = 40;
            this.label5.Text = "تومان";
            // 
            // txtBimar_NobatDoc_Bime
            // 
            this.txtBimar_NobatDoc_Bime.Location = new System.Drawing.Point(522, 120);
            this.txtBimar_NobatDoc_Bime.Name = "txtBimar_NobatDoc_Bime";
            this.txtBimar_NobatDoc_Bime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatDoc_Bime.TabIndex = 3;
            // 
            // txtlblBimar_NobatDoc_Bime
            // 
            this.txtlblBimar_NobatDoc_Bime.AutoSize = true;
            this.txtlblBimar_NobatDoc_Bime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_Bime.Location = new System.Drawing.Point(699, 101);
            this.txtlblBimar_NobatDoc_Bime.Name = "txtlblBimar_NobatDoc_Bime";
            this.txtlblBimar_NobatDoc_Bime.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatDoc_Bime.TabIndex = 2;
            this.txtlblBimar_NobatDoc_Bime.Text = "نوع بیمه :";
            // 
            // txtBimar_NobatDoc_Name
            // 
            this.txtBimar_NobatDoc_Name.Location = new System.Drawing.Point(524, 30);
            this.txtBimar_NobatDoc_Name.Name = "txtBimar_NobatDoc_Name";
            this.txtBimar_NobatDoc_Name.Size = new System.Drawing.Size(185, 23);
            this.txtBimar_NobatDoc_Name.TabIndex = 1;
            // 
            // txtlblBimar_NobatDoc_Name
            // 
            this.txtlblBimar_NobatDoc_Name.AutoSize = true;
            this.txtlblBimar_NobatDoc_Name.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_Name.Location = new System.Drawing.Point(645, 10);
            this.txtlblBimar_NobatDoc_Name.Name = "txtlblBimar_NobatDoc_Name";
            this.txtlblBimar_NobatDoc_Name.Size = new System.Drawing.Size(97, 17);
            this.txtlblBimar_NobatDoc_Name.TabIndex = 0;
            this.txtlblBimar_NobatDoc_Name.Text = "نام و نام خانوادگی بیمار :";
            // 
            // txtBimar_NobatDoc_MandePardakhti
            // 
            this.txtBimar_NobatDoc_MandePardakhti.Location = new System.Drawing.Point(44, 124);
            this.txtBimar_NobatDoc_MandePardakhti.Name = "txtBimar_NobatDoc_MandePardakhti";
            this.txtBimar_NobatDoc_MandePardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatDoc_MandePardakhti.TabIndex = 38;
            // 
            // txtBimar_NobatDoc_Pardakhti
            // 
            this.txtBimar_NobatDoc_Pardakhti.Location = new System.Drawing.Point(44, 78);
            this.txtBimar_NobatDoc_Pardakhti.Name = "txtBimar_NobatDoc_Pardakhti";
            this.txtBimar_NobatDoc_Pardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatDoc_Pardakhti.TabIndex = 37;
            // 
            // txtlblBimar_NobatDoc_ShomarePaziresh
            // 
            this.txtlblBimar_NobatDoc_ShomarePaziresh.AutoSize = true;
            this.txtlblBimar_NobatDoc_ShomarePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_ShomarePaziresh.Location = new System.Drawing.Point(288, 101);
            this.txtlblBimar_NobatDoc_ShomarePaziresh.Name = "txtlblBimar_NobatDoc_ShomarePaziresh";
            this.txtlblBimar_NobatDoc_ShomarePaziresh.Size = new System.Drawing.Size(30, 17);
            this.txtlblBimar_NobatDoc_ShomarePaziresh.TabIndex = 30;
            this.txtlblBimar_NobatDoc_ShomarePaziresh.Text = "نوبت :";
            // 
            // txtBimar_NobatDoc_ShomarePaziresh
            // 
            this.txtBimar_NobatDoc_ShomarePaziresh.Location = new System.Drawing.Point(264, 121);
            this.txtBimar_NobatDoc_ShomarePaziresh.Name = "txtBimar_NobatDoc_ShomarePaziresh";
            this.txtBimar_NobatDoc_ShomarePaziresh.Size = new System.Drawing.Size(51, 23);
            this.txtBimar_NobatDoc_ShomarePaziresh.TabIndex = 31;
            // 
            // txtlblBimar_NobatDoc_Pardakhti
            // 
            this.txtlblBimar_NobatDoc_Pardakhti.AutoSize = true;
            this.txtlblBimar_NobatDoc_Pardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_Pardakhti.Location = new System.Drawing.Point(189, 57);
            this.txtlblBimar_NobatDoc_Pardakhti.Name = "txtlblBimar_NobatDoc_Pardakhti";
            this.txtlblBimar_NobatDoc_Pardakhti.Size = new System.Drawing.Size(45, 17);
            this.txtlblBimar_NobatDoc_Pardakhti.TabIndex = 36;
            this.txtlblBimar_NobatDoc_Pardakhti.Text = "پرداختی :";
            // 
            // dateBimar_NobatDoc_DatePaziresh
            // 
            this.dateBimar_NobatDoc_DatePaziresh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateBimar_NobatDoc_DatePaziresh.Location = new System.Drawing.Point(334, 121);
            this.dateBimar_NobatDoc_DatePaziresh.Name = "dateBimar_NobatDoc_DatePaziresh";
            this.dateBimar_NobatDoc_DatePaziresh.Size = new System.Drawing.Size(115, 23);
            this.dateBimar_NobatDoc_DatePaziresh.TabIndex = 29;
            // 
            // txtBimar_NobatDoc_Mablagh
            // 
            this.txtBimar_NobatDoc_Mablagh.Location = new System.Drawing.Point(44, 32);
            this.txtBimar_NobatDoc_Mablagh.Name = "txtBimar_NobatDoc_Mablagh";
            this.txtBimar_NobatDoc_Mablagh.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatDoc_Mablagh.TabIndex = 32;
            // 
            // txtlblBimar_NobatDoc_MandePardakhti
            // 
            this.txtlblBimar_NobatDoc_MandePardakhti.AutoSize = true;
            this.txtlblBimar_NobatDoc_MandePardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_MandePardakhti.Location = new System.Drawing.Point(168, 104);
            this.txtlblBimar_NobatDoc_MandePardakhti.Name = "txtlblBimar_NobatDoc_MandePardakhti";
            this.txtlblBimar_NobatDoc_MandePardakhti.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatDoc_MandePardakhti.TabIndex = 35;
            this.txtlblBimar_NobatDoc_MandePardakhti.Text = "مانده پرداختی :";
            // 
            // txtlblBimar_NobatDoc_DatePaziresh
            // 
            this.txtlblBimar_NobatDoc_DatePaziresh.AutoSize = true;
            this.txtlblBimar_NobatDoc_DatePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_DatePaziresh.Location = new System.Drawing.Point(421, 101);
            this.txtlblBimar_NobatDoc_DatePaziresh.Name = "txtlblBimar_NobatDoc_DatePaziresh";
            this.txtlblBimar_NobatDoc_DatePaziresh.Size = new System.Drawing.Size(61, 17);
            this.txtlblBimar_NobatDoc_DatePaziresh.TabIndex = 28;
            this.txtlblBimar_NobatDoc_DatePaziresh.Text = "تاریخ پذیرش :";
            // 
            // txtlblBimar_NobatDoc_ChooseDoc
            // 
            this.txtlblBimar_NobatDoc_ChooseDoc.AutoSize = true;
            this.txtlblBimar_NobatDoc_ChooseDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_ChooseDoc.Location = new System.Drawing.Point(416, 55);
            this.txtlblBimar_NobatDoc_ChooseDoc.Name = "txtlblBimar_NobatDoc_ChooseDoc";
            this.txtlblBimar_NobatDoc_ChooseDoc.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatDoc_ChooseDoc.TabIndex = 26;
            this.txtlblBimar_NobatDoc_ChooseDoc.Text = "انتخاب پزشک :";
            // 
            // txtlblBimar_NobatDoc_Mablagh
            // 
            this.txtlblBimar_NobatDoc_Mablagh.AutoSize = true;
            this.txtlblBimar_NobatDoc_Mablagh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatDoc_Mablagh.Location = new System.Drawing.Point(175, 12);
            this.txtlblBimar_NobatDoc_Mablagh.Name = "txtlblBimar_NobatDoc_Mablagh";
            this.txtlblBimar_NobatDoc_Mablagh.Size = new System.Drawing.Size(59, 17);
            this.txtlblBimar_NobatDoc_Mablagh.TabIndex = 33;
            this.txtlblBimar_NobatDoc_Mablagh.Text = "قابل پرداخت :";
            // 
            // cmbBimar_NobatDoc_ChooseDoc
            // 
            this.cmbBimar_NobatDoc_ChooseDoc.FormattingEnabled = true;
            this.cmbBimar_NobatDoc_ChooseDoc.Location = new System.Drawing.Point(264, 73);
            this.cmbBimar_NobatDoc_ChooseDoc.Name = "cmbBimar_NobatDoc_ChooseDoc";
            this.cmbBimar_NobatDoc_ChooseDoc.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatDoc_ChooseDoc.TabIndex = 27;
            // 
            // tbBimar_NobatKhadamat
            // 
            this.tbBimar_NobatKhadamat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_KodeBime);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Mobile);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Mobile);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_KodeBime);
            this.tbBimar_NobatKhadamat.Controls.Add(this.label11);
            this.tbBimar_NobatKhadamat.Controls.Add(this.cmbBimar_NobatKhadamat_ChooseKhedmat);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_ChooseKhedmat);
            this.tbBimar_NobatKhadamat.Controls.Add(this.btnBimar_NobatKhadamat_Print);
            this.tbBimar_NobatKhadamat.Controls.Add(this.label13);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Kodemelli);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Kodemelli);
            this.tbBimar_NobatKhadamat.Controls.Add(this.btnBimar_NobatKhadamat_Save);
            this.tbBimar_NobatKhadamat.Controls.Add(this.label15);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Bime);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Bime);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Name);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Name);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_MandePardakhti);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Pardakhti);
            this.tbBimar_NobatKhadamat.Controls.Add(this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_ShomarePaziresh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Pardakhti);
            this.tbBimar_NobatKhadamat.Controls.Add(this.dateBimar_NobatKhadamat_DatePaziresh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtBimar_NobatKhadamat_Mablagh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_MandePardakhti);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_DatePaziresh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_ChooseDoc);
            this.tbBimar_NobatKhadamat.Controls.Add(this.txtlblBimar_NobatKhadamat_Mablagh);
            this.tbBimar_NobatKhadamat.Controls.Add(this.cmbBimar_NobatKhadamat_ChooseDoc);
            this.tbBimar_NobatKhadamat.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tbBimar_NobatKhadamat.ImageIndex = 3;
            this.tbBimar_NobatKhadamat.Location = new System.Drawing.Point(4, 24);
            this.tbBimar_NobatKhadamat.Name = "tbBimar_NobatKhadamat";
            this.tbBimar_NobatKhadamat.Padding = new System.Windows.Forms.Padding(3);
            this.tbBimar_NobatKhadamat.Size = new System.Drawing.Size(756, 197);
            this.tbBimar_NobatKhadamat.TabIndex = 1;
            this.tbBimar_NobatKhadamat.Text = "خدمات سرپایی";
            this.tbBimar_NobatKhadamat.UseVisualStyleBackColor = true;
            // 
            // txtlblBimar_NobatKhadamat_KodeBime
            // 
            this.txtlblBimar_NobatKhadamat_KodeBime.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_KodeBime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_KodeBime.Location = new System.Drawing.Point(679, 144);
            this.txtlblBimar_NobatKhadamat_KodeBime.Name = "txtlblBimar_NobatKhadamat_KodeBime";
            this.txtlblBimar_NobatKhadamat_KodeBime.Size = new System.Drawing.Size(65, 17);
            this.txtlblBimar_NobatKhadamat_KodeBime.TabIndex = 75;
            this.txtlblBimar_NobatKhadamat_KodeBime.Text = "شماره دفترچه :";
            // 
            // txtBimar_NobatKhadamat_Mobile
            // 
            this.txtBimar_NobatKhadamat_Mobile.Location = new System.Drawing.Point(267, 164);
            this.txtBimar_NobatKhadamat_Mobile.Name = "txtBimar_NobatKhadamat_Mobile";
            this.txtBimar_NobatKhadamat_Mobile.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatKhadamat_Mobile.TabIndex = 73;
            // 
            // txtlblBimar_NobatKhadamat_Mobile
            // 
            this.txtlblBimar_NobatKhadamat_Mobile.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Mobile.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Mobile.Location = new System.Drawing.Point(432, 144);
            this.txtlblBimar_NobatKhadamat_Mobile.Name = "txtlblBimar_NobatKhadamat_Mobile";
            this.txtlblBimar_NobatKhadamat_Mobile.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatKhadamat_Mobile.TabIndex = 72;
            this.txtlblBimar_NobatKhadamat_Mobile.Text = "تلفن همراه :";
            // 
            // txtBimar_NobatKhadamat_KodeBime
            // 
            this.txtBimar_NobatKhadamat_KodeBime.Location = new System.Drawing.Point(523, 166);
            this.txtBimar_NobatKhadamat_KodeBime.Name = "txtBimar_NobatKhadamat_KodeBime";
            this.txtBimar_NobatKhadamat_KodeBime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatKhadamat_KodeBime.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 17);
            this.label11.TabIndex = 71;
            this.label11.Text = "تومان";
            // 
            // cmbBimar_NobatKhadamat_ChooseKhedmat
            // 
            this.cmbBimar_NobatKhadamat_ChooseKhedmat.FormattingEnabled = true;
            this.cmbBimar_NobatKhadamat_ChooseKhedmat.Location = new System.Drawing.Point(266, 25);
            this.cmbBimar_NobatKhadamat_ChooseKhedmat.Name = "cmbBimar_NobatKhadamat_ChooseKhedmat";
            this.cmbBimar_NobatKhadamat_ChooseKhedmat.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatKhadamat_ChooseKhedmat.TabIndex = 54;
            // 
            // txtlblBimar_NobatKhadamat_ChooseKhedmat
            // 
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.Location = new System.Drawing.Point(432, 7);
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.Name = "txtlblBimar_NobatKhadamat_ChooseKhedmat";
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.TabIndex = 53;
            this.txtlblBimar_NobatKhadamat_ChooseKhedmat.Text = "نوع خدمت :";
            // 
            // btnBimar_NobatKhadamat_Print
            // 
            this.btnBimar_NobatKhadamat_Print.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatKhadamat_Print.Image = ((System.Drawing.Image)(resources.GetObject("btnBimar_NobatKhadamat_Print.Image")));
            this.btnBimar_NobatKhadamat_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBimar_NobatKhadamat_Print.Location = new System.Drawing.Point(13, 159);
            this.btnBimar_NobatKhadamat_Print.Name = "btnBimar_NobatKhadamat_Print";
            this.btnBimar_NobatKhadamat_Print.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatKhadamat_Print.TabIndex = 68;
            this.btnBimar_NobatKhadamat_Print.Text = "پرینت نوبت";
            this.btnBimar_NobatKhadamat_Print.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBimar_NobatKhadamat_Print.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 17);
            this.label13.TabIndex = 70;
            this.label13.Text = "تومان";
            // 
            // txtBimar_NobatKhadamat_Kodemelli
            // 
            this.txtBimar_NobatKhadamat_Kodemelli.Location = new System.Drawing.Point(524, 71);
            this.txtBimar_NobatKhadamat_Kodemelli.Name = "txtBimar_NobatKhadamat_Kodemelli";
            this.txtBimar_NobatKhadamat_Kodemelli.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatKhadamat_Kodemelli.TabIndex = 52;
            // 
            // txtlblBimar_NobatKhadamat_Kodemelli
            // 
            this.txtlblBimar_NobatKhadamat_Kodemelli.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Kodemelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Kodemelli.Location = new System.Drawing.Point(701, 51);
            this.txtlblBimar_NobatKhadamat_Kodemelli.Name = "txtlblBimar_NobatKhadamat_Kodemelli";
            this.txtlblBimar_NobatKhadamat_Kodemelli.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatKhadamat_Kodemelli.TabIndex = 51;
            this.txtlblBimar_NobatKhadamat_Kodemelli.Text = "  کد ملی :";
            // 
            // btnBimar_NobatKhadamat_Save
            // 
            this.btnBimar_NobatKhadamat_Save.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatKhadamat_Save.Location = new System.Drawing.Point(135, 159);
            this.btnBimar_NobatKhadamat_Save.Name = "btnBimar_NobatKhadamat_Save";
            this.btnBimar_NobatKhadamat_Save.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatKhadamat_Save.TabIndex = 63;
            this.btnBimar_NobatKhadamat_Save.Text = "ثبت";
            this.btnBimar_NobatKhadamat_Save.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 17);
            this.label15.TabIndex = 69;
            this.label15.Text = "تومان";
            // 
            // txtBimar_NobatKhadamat_Bime
            // 
            this.txtBimar_NobatKhadamat_Bime.Location = new System.Drawing.Point(524, 118);
            this.txtBimar_NobatKhadamat_Bime.Name = "txtBimar_NobatKhadamat_Bime";
            this.txtBimar_NobatKhadamat_Bime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatKhadamat_Bime.TabIndex = 50;
            // 
            // txtlblBimar_NobatKhadamat_Bime
            // 
            this.txtlblBimar_NobatKhadamat_Bime.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Bime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Bime.Location = new System.Drawing.Point(701, 99);
            this.txtlblBimar_NobatKhadamat_Bime.Name = "txtlblBimar_NobatKhadamat_Bime";
            this.txtlblBimar_NobatKhadamat_Bime.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatKhadamat_Bime.TabIndex = 49;
            this.txtlblBimar_NobatKhadamat_Bime.Text = "نوع بیمه :";
            // 
            // txtBimar_NobatKhadamat_Name
            // 
            this.txtBimar_NobatKhadamat_Name.Location = new System.Drawing.Point(526, 28);
            this.txtBimar_NobatKhadamat_Name.Name = "txtBimar_NobatKhadamat_Name";
            this.txtBimar_NobatKhadamat_Name.Size = new System.Drawing.Size(185, 23);
            this.txtBimar_NobatKhadamat_Name.TabIndex = 48;
            // 
            // txtlblBimar_NobatKhadamat_Name
            // 
            this.txtlblBimar_NobatKhadamat_Name.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Name.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Name.Location = new System.Drawing.Point(647, 8);
            this.txtlblBimar_NobatKhadamat_Name.Name = "txtlblBimar_NobatKhadamat_Name";
            this.txtlblBimar_NobatKhadamat_Name.Size = new System.Drawing.Size(97, 17);
            this.txtlblBimar_NobatKhadamat_Name.TabIndex = 47;
            this.txtlblBimar_NobatKhadamat_Name.Text = "نام و نام خانوادگی بیمار :";
            // 
            // txtBimar_NobatKhadamat_MandePardakhti
            // 
            this.txtBimar_NobatKhadamat_MandePardakhti.Location = new System.Drawing.Point(46, 122);
            this.txtBimar_NobatKhadamat_MandePardakhti.Name = "txtBimar_NobatKhadamat_MandePardakhti";
            this.txtBimar_NobatKhadamat_MandePardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatKhadamat_MandePardakhti.TabIndex = 67;
            // 
            // txtBimar_NobatKhadamat_Pardakhti
            // 
            this.txtBimar_NobatKhadamat_Pardakhti.Location = new System.Drawing.Point(46, 76);
            this.txtBimar_NobatKhadamat_Pardakhti.Name = "txtBimar_NobatKhadamat_Pardakhti";
            this.txtBimar_NobatKhadamat_Pardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatKhadamat_Pardakhti.TabIndex = 66;
            // 
            // label18txtlblBimar_NobatKhadamat_ShomarePaziresh
            // 
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.AutoSize = true;
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.Location = new System.Drawing.Point(290, 99);
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.Name = "label18txtlblBimar_NobatKhadamat_ShomarePaziresh";
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.Size = new System.Drawing.Size(30, 17);
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.TabIndex = 59;
            this.label18txtlblBimar_NobatKhadamat_ShomarePaziresh.Text = "نوبت :";
            // 
            // txtBimar_NobatKhadamat_ShomarePaziresh
            // 
            this.txtBimar_NobatKhadamat_ShomarePaziresh.Location = new System.Drawing.Point(266, 119);
            this.txtBimar_NobatKhadamat_ShomarePaziresh.Name = "txtBimar_NobatKhadamat_ShomarePaziresh";
            this.txtBimar_NobatKhadamat_ShomarePaziresh.Size = new System.Drawing.Size(51, 23);
            this.txtBimar_NobatKhadamat_ShomarePaziresh.TabIndex = 60;
            // 
            // txtlblBimar_NobatKhadamat_Pardakhti
            // 
            this.txtlblBimar_NobatKhadamat_Pardakhti.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Pardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Pardakhti.Location = new System.Drawing.Point(191, 55);
            this.txtlblBimar_NobatKhadamat_Pardakhti.Name = "txtlblBimar_NobatKhadamat_Pardakhti";
            this.txtlblBimar_NobatKhadamat_Pardakhti.Size = new System.Drawing.Size(45, 17);
            this.txtlblBimar_NobatKhadamat_Pardakhti.TabIndex = 65;
            this.txtlblBimar_NobatKhadamat_Pardakhti.Text = "پرداختی :";
            // 
            // dateBimar_NobatKhadamat_DatePaziresh
            // 
            this.dateBimar_NobatKhadamat_DatePaziresh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateBimar_NobatKhadamat_DatePaziresh.Location = new System.Drawing.Point(336, 119);
            this.dateBimar_NobatKhadamat_DatePaziresh.Name = "dateBimar_NobatKhadamat_DatePaziresh";
            this.dateBimar_NobatKhadamat_DatePaziresh.Size = new System.Drawing.Size(115, 23);
            this.dateBimar_NobatKhadamat_DatePaziresh.TabIndex = 58;
            // 
            // txtBimar_NobatKhadamat_Mablagh
            // 
            this.txtBimar_NobatKhadamat_Mablagh.Location = new System.Drawing.Point(46, 30);
            this.txtBimar_NobatKhadamat_Mablagh.Name = "txtBimar_NobatKhadamat_Mablagh";
            this.txtBimar_NobatKhadamat_Mablagh.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatKhadamat_Mablagh.TabIndex = 61;
            // 
            // txtlblBimar_NobatKhadamat_MandePardakhti
            // 
            this.txtlblBimar_NobatKhadamat_MandePardakhti.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_MandePardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_MandePardakhti.Location = new System.Drawing.Point(170, 102);
            this.txtlblBimar_NobatKhadamat_MandePardakhti.Name = "txtlblBimar_NobatKhadamat_MandePardakhti";
            this.txtlblBimar_NobatKhadamat_MandePardakhti.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatKhadamat_MandePardakhti.TabIndex = 64;
            this.txtlblBimar_NobatKhadamat_MandePardakhti.Text = "مانده پرداختی :";
            // 
            // txtlblBimar_NobatKhadamat_DatePaziresh
            // 
            this.txtlblBimar_NobatKhadamat_DatePaziresh.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_DatePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_DatePaziresh.Location = new System.Drawing.Point(423, 99);
            this.txtlblBimar_NobatKhadamat_DatePaziresh.Name = "txtlblBimar_NobatKhadamat_DatePaziresh";
            this.txtlblBimar_NobatKhadamat_DatePaziresh.Size = new System.Drawing.Size(61, 17);
            this.txtlblBimar_NobatKhadamat_DatePaziresh.TabIndex = 57;
            this.txtlblBimar_NobatKhadamat_DatePaziresh.Text = "تاریخ پذیرش :";
            // 
            // txtlblBimar_NobatKhadamat_ChooseDoc
            // 
            this.txtlblBimar_NobatKhadamat_ChooseDoc.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_ChooseDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_ChooseDoc.Location = new System.Drawing.Point(418, 53);
            this.txtlblBimar_NobatKhadamat_ChooseDoc.Name = "txtlblBimar_NobatKhadamat_ChooseDoc";
            this.txtlblBimar_NobatKhadamat_ChooseDoc.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatKhadamat_ChooseDoc.TabIndex = 55;
            this.txtlblBimar_NobatKhadamat_ChooseDoc.Text = "انتخاب پزشک :";
            // 
            // txtlblBimar_NobatKhadamat_Mablagh
            // 
            this.txtlblBimar_NobatKhadamat_Mablagh.AutoSize = true;
            this.txtlblBimar_NobatKhadamat_Mablagh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatKhadamat_Mablagh.Location = new System.Drawing.Point(177, 10);
            this.txtlblBimar_NobatKhadamat_Mablagh.Name = "txtlblBimar_NobatKhadamat_Mablagh";
            this.txtlblBimar_NobatKhadamat_Mablagh.Size = new System.Drawing.Size(59, 17);
            this.txtlblBimar_NobatKhadamat_Mablagh.TabIndex = 62;
            this.txtlblBimar_NobatKhadamat_Mablagh.Text = "قابل پرداخت :";
            // 
            // cmbBimar_NobatKhadamat_ChooseDoc
            // 
            this.cmbBimar_NobatKhadamat_ChooseDoc.FormattingEnabled = true;
            this.cmbBimar_NobatKhadamat_ChooseDoc.Location = new System.Drawing.Point(266, 71);
            this.cmbBimar_NobatKhadamat_ChooseDoc.Name = "cmbBimar_NobatKhadamat_ChooseDoc";
            this.cmbBimar_NobatKhadamat_ChooseDoc.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatKhadamat_ChooseDoc.TabIndex = 56;
            // 
            // tbBimar_NobatScan
            // 
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_KodeBime);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Mobile);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Mobile);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_KodeBime);
            this.tbBimar_NobatScan.Controls.Add(this.label9);
            this.tbBimar_NobatScan.Controls.Add(this.cmbBimar_NobatScan_ChooseKhedmat);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_ChooseKhedmat);
            this.tbBimar_NobatScan.Controls.Add(this.btnBimar_NobatScan_Print);
            this.tbBimar_NobatScan.Controls.Add(this.label22);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Kodemelli);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Kodemelli);
            this.tbBimar_NobatScan.Controls.Add(this.btnBimar_NobatScan_Save);
            this.tbBimar_NobatScan.Controls.Add(this.label24);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Bime);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Bime);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Name);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Name);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_MandePardakhti);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Pardakhti);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_ShomarePaziresh);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_ShomarePaziresh);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Pardakhti);
            this.tbBimar_NobatScan.Controls.Add(this.dateBimar_NobatScan_DatePaziresh);
            this.tbBimar_NobatScan.Controls.Add(this.txtBimar_NobatScan_Mablagh);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_MandePardakhti);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_DatePaziresh);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_ChooseDoc);
            this.tbBimar_NobatScan.Controls.Add(this.txtlblBimar_NobatScan_Mablagh);
            this.tbBimar_NobatScan.Controls.Add(this.cmbBimar_NobatScan_ChooseDoc);
            this.tbBimar_NobatScan.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tbBimar_NobatScan.ImageIndex = 2;
            this.tbBimar_NobatScan.Location = new System.Drawing.Point(4, 24);
            this.tbBimar_NobatScan.Name = "tbBimar_NobatScan";
            this.tbBimar_NobatScan.Size = new System.Drawing.Size(756, 197);
            this.tbBimar_NobatScan.TabIndex = 2;
            this.tbBimar_NobatScan.Text = "تصویر برداری";
            this.tbBimar_NobatScan.UseVisualStyleBackColor = true;
            // 
            // txtlblBimar_NobatScan_KodeBime
            // 
            this.txtlblBimar_NobatScan_KodeBime.AutoSize = true;
            this.txtlblBimar_NobatScan_KodeBime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_KodeBime.Location = new System.Drawing.Point(679, 144);
            this.txtlblBimar_NobatScan_KodeBime.Name = "txtlblBimar_NobatScan_KodeBime";
            this.txtlblBimar_NobatScan_KodeBime.Size = new System.Drawing.Size(65, 17);
            this.txtlblBimar_NobatScan_KodeBime.TabIndex = 75;
            this.txtlblBimar_NobatScan_KodeBime.Text = "شماره دفترچه :";
            // 
            // txtBimar_NobatScan_Mobile
            // 
            this.txtBimar_NobatScan_Mobile.Location = new System.Drawing.Point(267, 164);
            this.txtBimar_NobatScan_Mobile.Name = "txtBimar_NobatScan_Mobile";
            this.txtBimar_NobatScan_Mobile.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatScan_Mobile.TabIndex = 73;
            // 
            // txtlblBimar_NobatScan_Mobile
            // 
            this.txtlblBimar_NobatScan_Mobile.AutoSize = true;
            this.txtlblBimar_NobatScan_Mobile.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Mobile.Location = new System.Drawing.Point(432, 144);
            this.txtlblBimar_NobatScan_Mobile.Name = "txtlblBimar_NobatScan_Mobile";
            this.txtlblBimar_NobatScan_Mobile.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatScan_Mobile.TabIndex = 72;
            this.txtlblBimar_NobatScan_Mobile.Text = "تلفن همراه :";
            // 
            // txtBimar_NobatScan_KodeBime
            // 
            this.txtBimar_NobatScan_KodeBime.Location = new System.Drawing.Point(523, 166);
            this.txtBimar_NobatScan_KodeBime.Name = "txtBimar_NobatScan_KodeBime";
            this.txtBimar_NobatScan_KodeBime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatScan_KodeBime.TabIndex = 74;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 17);
            this.label9.TabIndex = 71;
            this.label9.Text = "تومان";
            // 
            // cmbBimar_NobatScan_ChooseKhedmat
            // 
            this.cmbBimar_NobatScan_ChooseKhedmat.FormattingEnabled = true;
            this.cmbBimar_NobatScan_ChooseKhedmat.Location = new System.Drawing.Point(266, 25);
            this.cmbBimar_NobatScan_ChooseKhedmat.Name = "cmbBimar_NobatScan_ChooseKhedmat";
            this.cmbBimar_NobatScan_ChooseKhedmat.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatScan_ChooseKhedmat.TabIndex = 54;
            // 
            // txtlblBimar_NobatScan_ChooseKhedmat
            // 
            this.txtlblBimar_NobatScan_ChooseKhedmat.AutoSize = true;
            this.txtlblBimar_NobatScan_ChooseKhedmat.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_ChooseKhedmat.Location = new System.Drawing.Point(432, 7);
            this.txtlblBimar_NobatScan_ChooseKhedmat.Name = "txtlblBimar_NobatScan_ChooseKhedmat";
            this.txtlblBimar_NobatScan_ChooseKhedmat.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatScan_ChooseKhedmat.TabIndex = 53;
            this.txtlblBimar_NobatScan_ChooseKhedmat.Text = "نوع خدمت :";
            // 
            // btnBimar_NobatScan_Print
            // 
            this.btnBimar_NobatScan_Print.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatScan_Print.Image = ((System.Drawing.Image)(resources.GetObject("btnBimar_NobatScan_Print.Image")));
            this.btnBimar_NobatScan_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBimar_NobatScan_Print.Location = new System.Drawing.Point(13, 159);
            this.btnBimar_NobatScan_Print.Name = "btnBimar_NobatScan_Print";
            this.btnBimar_NobatScan_Print.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatScan_Print.TabIndex = 68;
            this.btnBimar_NobatScan_Print.Text = "پرینت نوبت";
            this.btnBimar_NobatScan_Print.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBimar_NobatScan_Print.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 79);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 17);
            this.label22.TabIndex = 70;
            this.label22.Text = "تومان";
            // 
            // txtBimar_NobatScan_Kodemelli
            // 
            this.txtBimar_NobatScan_Kodemelli.Location = new System.Drawing.Point(524, 71);
            this.txtBimar_NobatScan_Kodemelli.Name = "txtBimar_NobatScan_Kodemelli";
            this.txtBimar_NobatScan_Kodemelli.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatScan_Kodemelli.TabIndex = 52;
            // 
            // txtlblBimar_NobatScan_Kodemelli
            // 
            this.txtlblBimar_NobatScan_Kodemelli.AutoSize = true;
            this.txtlblBimar_NobatScan_Kodemelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Kodemelli.Location = new System.Drawing.Point(701, 51);
            this.txtlblBimar_NobatScan_Kodemelli.Name = "txtlblBimar_NobatScan_Kodemelli";
            this.txtlblBimar_NobatScan_Kodemelli.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatScan_Kodemelli.TabIndex = 51;
            this.txtlblBimar_NobatScan_Kodemelli.Text = "  کد ملی :";
            // 
            // btnBimar_NobatScan_Save
            // 
            this.btnBimar_NobatScan_Save.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatScan_Save.Location = new System.Drawing.Point(135, 159);
            this.btnBimar_NobatScan_Save.Name = "btnBimar_NobatScan_Save";
            this.btnBimar_NobatScan_Save.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatScan_Save.TabIndex = 63;
            this.btnBimar_NobatScan_Save.Text = "ثبت";
            this.btnBimar_NobatScan_Save.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 33);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 17);
            this.label24.TabIndex = 69;
            this.label24.Text = "تومان";
            // 
            // txtBimar_NobatScan_Bime
            // 
            this.txtBimar_NobatScan_Bime.Location = new System.Drawing.Point(524, 118);
            this.txtBimar_NobatScan_Bime.Name = "txtBimar_NobatScan_Bime";
            this.txtBimar_NobatScan_Bime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatScan_Bime.TabIndex = 50;
            // 
            // txtlblBimar_NobatScan_Bime
            // 
            this.txtlblBimar_NobatScan_Bime.AutoSize = true;
            this.txtlblBimar_NobatScan_Bime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Bime.Location = new System.Drawing.Point(701, 99);
            this.txtlblBimar_NobatScan_Bime.Name = "txtlblBimar_NobatScan_Bime";
            this.txtlblBimar_NobatScan_Bime.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatScan_Bime.TabIndex = 49;
            this.txtlblBimar_NobatScan_Bime.Text = "نوع بیمه :";
            // 
            // txtBimar_NobatScan_Name
            // 
            this.txtBimar_NobatScan_Name.Location = new System.Drawing.Point(526, 28);
            this.txtBimar_NobatScan_Name.Name = "txtBimar_NobatScan_Name";
            this.txtBimar_NobatScan_Name.Size = new System.Drawing.Size(185, 23);
            this.txtBimar_NobatScan_Name.TabIndex = 48;
            // 
            // txtlblBimar_NobatScan_Name
            // 
            this.txtlblBimar_NobatScan_Name.AutoSize = true;
            this.txtlblBimar_NobatScan_Name.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Name.Location = new System.Drawing.Point(647, 8);
            this.txtlblBimar_NobatScan_Name.Name = "txtlblBimar_NobatScan_Name";
            this.txtlblBimar_NobatScan_Name.Size = new System.Drawing.Size(97, 17);
            this.txtlblBimar_NobatScan_Name.TabIndex = 47;
            this.txtlblBimar_NobatScan_Name.Text = "نام و نام خانوادگی بیمار :";
            // 
            // txtBimar_NobatScan_MandePardakhti
            // 
            this.txtBimar_NobatScan_MandePardakhti.Location = new System.Drawing.Point(46, 122);
            this.txtBimar_NobatScan_MandePardakhti.Name = "txtBimar_NobatScan_MandePardakhti";
            this.txtBimar_NobatScan_MandePardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatScan_MandePardakhti.TabIndex = 67;
            // 
            // txtBimar_NobatScan_Pardakhti
            // 
            this.txtBimar_NobatScan_Pardakhti.Location = new System.Drawing.Point(46, 76);
            this.txtBimar_NobatScan_Pardakhti.Name = "txtBimar_NobatScan_Pardakhti";
            this.txtBimar_NobatScan_Pardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatScan_Pardakhti.TabIndex = 66;
            // 
            // txtlblBimar_NobatScan_ShomarePaziresh
            // 
            this.txtlblBimar_NobatScan_ShomarePaziresh.AutoSize = true;
            this.txtlblBimar_NobatScan_ShomarePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_ShomarePaziresh.Location = new System.Drawing.Point(290, 99);
            this.txtlblBimar_NobatScan_ShomarePaziresh.Name = "txtlblBimar_NobatScan_ShomarePaziresh";
            this.txtlblBimar_NobatScan_ShomarePaziresh.Size = new System.Drawing.Size(30, 17);
            this.txtlblBimar_NobatScan_ShomarePaziresh.TabIndex = 59;
            this.txtlblBimar_NobatScan_ShomarePaziresh.Text = "نوبت :";
            // 
            // txtBimar_NobatScan_ShomarePaziresh
            // 
            this.txtBimar_NobatScan_ShomarePaziresh.Location = new System.Drawing.Point(266, 119);
            this.txtBimar_NobatScan_ShomarePaziresh.Name = "txtBimar_NobatScan_ShomarePaziresh";
            this.txtBimar_NobatScan_ShomarePaziresh.Size = new System.Drawing.Size(51, 23);
            this.txtBimar_NobatScan_ShomarePaziresh.TabIndex = 60;
            // 
            // txtlblBimar_NobatScan_Pardakhti
            // 
            this.txtlblBimar_NobatScan_Pardakhti.AutoSize = true;
            this.txtlblBimar_NobatScan_Pardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Pardakhti.Location = new System.Drawing.Point(191, 55);
            this.txtlblBimar_NobatScan_Pardakhti.Name = "txtlblBimar_NobatScan_Pardakhti";
            this.txtlblBimar_NobatScan_Pardakhti.Size = new System.Drawing.Size(45, 17);
            this.txtlblBimar_NobatScan_Pardakhti.TabIndex = 65;
            this.txtlblBimar_NobatScan_Pardakhti.Text = "پرداختی :";
            // 
            // dateBimar_NobatScan_DatePaziresh
            // 
            this.dateBimar_NobatScan_DatePaziresh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateBimar_NobatScan_DatePaziresh.Location = new System.Drawing.Point(336, 119);
            this.dateBimar_NobatScan_DatePaziresh.Name = "dateBimar_NobatScan_DatePaziresh";
            this.dateBimar_NobatScan_DatePaziresh.Size = new System.Drawing.Size(115, 23);
            this.dateBimar_NobatScan_DatePaziresh.TabIndex = 58;
            // 
            // txtBimar_NobatScan_Mablagh
            // 
            this.txtBimar_NobatScan_Mablagh.Location = new System.Drawing.Point(46, 30);
            this.txtBimar_NobatScan_Mablagh.Name = "txtBimar_NobatScan_Mablagh";
            this.txtBimar_NobatScan_Mablagh.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatScan_Mablagh.TabIndex = 61;
            // 
            // txtlblBimar_NobatScan_MandePardakhti
            // 
            this.txtlblBimar_NobatScan_MandePardakhti.AutoSize = true;
            this.txtlblBimar_NobatScan_MandePardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_MandePardakhti.Location = new System.Drawing.Point(170, 102);
            this.txtlblBimar_NobatScan_MandePardakhti.Name = "txtlblBimar_NobatScan_MandePardakhti";
            this.txtlblBimar_NobatScan_MandePardakhti.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatScan_MandePardakhti.TabIndex = 64;
            this.txtlblBimar_NobatScan_MandePardakhti.Text = "مانده پرداختی :";
            // 
            // txtlblBimar_NobatScan_DatePaziresh
            // 
            this.txtlblBimar_NobatScan_DatePaziresh.AutoSize = true;
            this.txtlblBimar_NobatScan_DatePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_DatePaziresh.Location = new System.Drawing.Point(423, 99);
            this.txtlblBimar_NobatScan_DatePaziresh.Name = "txtlblBimar_NobatScan_DatePaziresh";
            this.txtlblBimar_NobatScan_DatePaziresh.Size = new System.Drawing.Size(61, 17);
            this.txtlblBimar_NobatScan_DatePaziresh.TabIndex = 57;
            this.txtlblBimar_NobatScan_DatePaziresh.Text = "تاریخ پذیرش :";
            // 
            // txtlblBimar_NobatScan_ChooseDoc
            // 
            this.txtlblBimar_NobatScan_ChooseDoc.AutoSize = true;
            this.txtlblBimar_NobatScan_ChooseDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_ChooseDoc.Location = new System.Drawing.Point(418, 53);
            this.txtlblBimar_NobatScan_ChooseDoc.Name = "txtlblBimar_NobatScan_ChooseDoc";
            this.txtlblBimar_NobatScan_ChooseDoc.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatScan_ChooseDoc.TabIndex = 55;
            this.txtlblBimar_NobatScan_ChooseDoc.Text = "انتخاب پزشک :";
            // 
            // txtlblBimar_NobatScan_Mablagh
            // 
            this.txtlblBimar_NobatScan_Mablagh.AutoSize = true;
            this.txtlblBimar_NobatScan_Mablagh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatScan_Mablagh.Location = new System.Drawing.Point(177, 10);
            this.txtlblBimar_NobatScan_Mablagh.Name = "txtlblBimar_NobatScan_Mablagh";
            this.txtlblBimar_NobatScan_Mablagh.Size = new System.Drawing.Size(59, 17);
            this.txtlblBimar_NobatScan_Mablagh.TabIndex = 62;
            this.txtlblBimar_NobatScan_Mablagh.Text = "قابل پرداخت :";
            // 
            // cmbBimar_NobatScan_ChooseDoc
            // 
            this.cmbBimar_NobatScan_ChooseDoc.FormattingEnabled = true;
            this.cmbBimar_NobatScan_ChooseDoc.Location = new System.Drawing.Point(266, 71);
            this.cmbBimar_NobatScan_ChooseDoc.Name = "cmbBimar_NobatScan_ChooseDoc";
            this.cmbBimar_NobatScan_ChooseDoc.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatScan_ChooseDoc.TabIndex = 56;
            // 
            // tbBimar_NobatLab
            // 
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_KodeBime);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Mobile);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Mobile);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_KodeBime);
            this.tbBimar_NobatLab.Controls.Add(this.label35);
            this.tbBimar_NobatLab.Controls.Add(this.cmbBimar_NobatLab_ChooseKhedmat);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_ChooseKhedmat);
            this.tbBimar_NobatLab.Controls.Add(this.btnBimar_NobatLab_Print);
            this.tbBimar_NobatLab.Controls.Add(this.label37);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Kodemelli);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Kodemelli);
            this.tbBimar_NobatLab.Controls.Add(this.btnBimar_NobatLab_Save);
            this.tbBimar_NobatLab.Controls.Add(this.label39);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Bime);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Bime);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Name);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Name);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_MandePardakhti);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Pardakhti);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_ShomarePaziresh);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_ShomarePaziresh);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Pardakhti);
            this.tbBimar_NobatLab.Controls.Add(this.dateBimar_NobatLab_DatePaziresh);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_Mablagh);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_MandePardakhti);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_DatePaziresh);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_ChooseDoc);
            this.tbBimar_NobatLab.Controls.Add(this.txtlblBimar_NobatLab_Mablagh);
            this.tbBimar_NobatLab.Controls.Add(this.txtBimar_NobatLab_ChooseDoc);
            this.tbBimar_NobatLab.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tbBimar_NobatLab.ImageIndex = 3;
            this.tbBimar_NobatLab.Location = new System.Drawing.Point(4, 24);
            this.tbBimar_NobatLab.Name = "tbBimar_NobatLab";
            this.tbBimar_NobatLab.Padding = new System.Windows.Forms.Padding(3);
            this.tbBimar_NobatLab.Size = new System.Drawing.Size(756, 197);
            this.tbBimar_NobatLab.TabIndex = 3;
            this.tbBimar_NobatLab.Text = "آزمایشگاه";
            this.tbBimar_NobatLab.UseVisualStyleBackColor = true;
            // 
            // txtlblBimar_NobatLab_KodeBime
            // 
            this.txtlblBimar_NobatLab_KodeBime.AutoSize = true;
            this.txtlblBimar_NobatLab_KodeBime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_KodeBime.Location = new System.Drawing.Point(679, 144);
            this.txtlblBimar_NobatLab_KodeBime.Name = "txtlblBimar_NobatLab_KodeBime";
            this.txtlblBimar_NobatLab_KodeBime.Size = new System.Drawing.Size(65, 17);
            this.txtlblBimar_NobatLab_KodeBime.TabIndex = 75;
            this.txtlblBimar_NobatLab_KodeBime.Text = "شماره دفترچه :";
            // 
            // txtBimar_NobatLab_Mobile
            // 
            this.txtBimar_NobatLab_Mobile.Location = new System.Drawing.Point(267, 164);
            this.txtBimar_NobatLab_Mobile.Name = "txtBimar_NobatLab_Mobile";
            this.txtBimar_NobatLab_Mobile.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatLab_Mobile.TabIndex = 73;
            // 
            // txtlblBimar_NobatLab_Mobile
            // 
            this.txtlblBimar_NobatLab_Mobile.AutoSize = true;
            this.txtlblBimar_NobatLab_Mobile.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Mobile.Location = new System.Drawing.Point(432, 144);
            this.txtlblBimar_NobatLab_Mobile.Name = "txtlblBimar_NobatLab_Mobile";
            this.txtlblBimar_NobatLab_Mobile.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatLab_Mobile.TabIndex = 72;
            this.txtlblBimar_NobatLab_Mobile.Text = "تلفن همراه :";
            // 
            // txtBimar_NobatLab_KodeBime
            // 
            this.txtBimar_NobatLab_KodeBime.Location = new System.Drawing.Point(523, 166);
            this.txtBimar_NobatLab_KodeBime.Name = "txtBimar_NobatLab_KodeBime";
            this.txtBimar_NobatLab_KodeBime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatLab_KodeBime.TabIndex = 74;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 125);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(27, 17);
            this.label35.TabIndex = 71;
            this.label35.Text = "تومان";
            // 
            // cmbBimar_NobatLab_ChooseKhedmat
            // 
            this.cmbBimar_NobatLab_ChooseKhedmat.FormattingEnabled = true;
            this.cmbBimar_NobatLab_ChooseKhedmat.Location = new System.Drawing.Point(266, 25);
            this.cmbBimar_NobatLab_ChooseKhedmat.Name = "cmbBimar_NobatLab_ChooseKhedmat";
            this.cmbBimar_NobatLab_ChooseKhedmat.Size = new System.Drawing.Size(185, 25);
            this.cmbBimar_NobatLab_ChooseKhedmat.TabIndex = 54;
            // 
            // txtlblBimar_NobatLab_ChooseKhedmat
            // 
            this.txtlblBimar_NobatLab_ChooseKhedmat.AutoSize = true;
            this.txtlblBimar_NobatLab_ChooseKhedmat.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_ChooseKhedmat.Location = new System.Drawing.Point(432, 7);
            this.txtlblBimar_NobatLab_ChooseKhedmat.Name = "txtlblBimar_NobatLab_ChooseKhedmat";
            this.txtlblBimar_NobatLab_ChooseKhedmat.Size = new System.Drawing.Size(52, 17);
            this.txtlblBimar_NobatLab_ChooseKhedmat.TabIndex = 53;
            this.txtlblBimar_NobatLab_ChooseKhedmat.Text = "نوع خدمت :";
            // 
            // btnBimar_NobatLab_Print
            // 
            this.btnBimar_NobatLab_Print.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatLab_Print.Image = ((System.Drawing.Image)(resources.GetObject("btnBimar_NobatLab_Print.Image")));
            this.btnBimar_NobatLab_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBimar_NobatLab_Print.Location = new System.Drawing.Point(13, 159);
            this.btnBimar_NobatLab_Print.Name = "btnBimar_NobatLab_Print";
            this.btnBimar_NobatLab_Print.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatLab_Print.TabIndex = 68;
            this.btnBimar_NobatLab_Print.Text = "پرینت نوبت";
            this.btnBimar_NobatLab_Print.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBimar_NobatLab_Print.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(13, 79);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 17);
            this.label37.TabIndex = 70;
            this.label37.Text = "تومان";
            // 
            // txtBimar_NobatLab_Kodemelli
            // 
            this.txtBimar_NobatLab_Kodemelli.Location = new System.Drawing.Point(524, 71);
            this.txtBimar_NobatLab_Kodemelli.Name = "txtBimar_NobatLab_Kodemelli";
            this.txtBimar_NobatLab_Kodemelli.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatLab_Kodemelli.TabIndex = 52;
            // 
            // txtlblBimar_NobatLab_Kodemelli
            // 
            this.txtlblBimar_NobatLab_Kodemelli.AutoSize = true;
            this.txtlblBimar_NobatLab_Kodemelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Kodemelli.Location = new System.Drawing.Point(701, 51);
            this.txtlblBimar_NobatLab_Kodemelli.Name = "txtlblBimar_NobatLab_Kodemelli";
            this.txtlblBimar_NobatLab_Kodemelli.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatLab_Kodemelli.TabIndex = 51;
            this.txtlblBimar_NobatLab_Kodemelli.Text = "  کد ملی :";
            // 
            // btnBimar_NobatLab_Save
            // 
            this.btnBimar_NobatLab_Save.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBimar_NobatLab_Save.Location = new System.Drawing.Point(135, 159);
            this.btnBimar_NobatLab_Save.Name = "btnBimar_NobatLab_Save";
            this.btnBimar_NobatLab_Save.Size = new System.Drawing.Size(100, 30);
            this.btnBimar_NobatLab_Save.TabIndex = 63;
            this.btnBimar_NobatLab_Save.Text = "ثبت";
            this.btnBimar_NobatLab_Save.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(13, 33);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(27, 17);
            this.label39.TabIndex = 69;
            this.label39.Text = "تومان";
            // 
            // txtBimar_NobatLab_Bime
            // 
            this.txtBimar_NobatLab_Bime.Location = new System.Drawing.Point(524, 118);
            this.txtBimar_NobatLab_Bime.Name = "txtBimar_NobatLab_Bime";
            this.txtBimar_NobatLab_Bime.Size = new System.Drawing.Size(184, 23);
            this.txtBimar_NobatLab_Bime.TabIndex = 50;
            // 
            // txtlblBimar_NobatLab_Bime
            // 
            this.txtlblBimar_NobatLab_Bime.AutoSize = true;
            this.txtlblBimar_NobatLab_Bime.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Bime.Location = new System.Drawing.Point(701, 99);
            this.txtlblBimar_NobatLab_Bime.Name = "txtlblBimar_NobatLab_Bime";
            this.txtlblBimar_NobatLab_Bime.Size = new System.Drawing.Size(43, 17);
            this.txtlblBimar_NobatLab_Bime.TabIndex = 49;
            this.txtlblBimar_NobatLab_Bime.Text = "نوع بیمه :";
            // 
            // txtBimar_NobatLab_Name
            // 
            this.txtBimar_NobatLab_Name.Location = new System.Drawing.Point(526, 28);
            this.txtBimar_NobatLab_Name.Name = "txtBimar_NobatLab_Name";
            this.txtBimar_NobatLab_Name.Size = new System.Drawing.Size(185, 23);
            this.txtBimar_NobatLab_Name.TabIndex = 48;
            // 
            // txtlblBimar_NobatLab_Name
            // 
            this.txtlblBimar_NobatLab_Name.AutoSize = true;
            this.txtlblBimar_NobatLab_Name.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Name.Location = new System.Drawing.Point(647, 8);
            this.txtlblBimar_NobatLab_Name.Name = "txtlblBimar_NobatLab_Name";
            this.txtlblBimar_NobatLab_Name.Size = new System.Drawing.Size(97, 17);
            this.txtlblBimar_NobatLab_Name.TabIndex = 47;
            this.txtlblBimar_NobatLab_Name.Text = "نام و نام خانوادگی بیمار :";
            // 
            // txtBimar_NobatLab_MandePardakhti
            // 
            this.txtBimar_NobatLab_MandePardakhti.Location = new System.Drawing.Point(46, 122);
            this.txtBimar_NobatLab_MandePardakhti.Name = "txtBimar_NobatLab_MandePardakhti";
            this.txtBimar_NobatLab_MandePardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatLab_MandePardakhti.TabIndex = 67;
            // 
            // txtBimar_NobatLab_Pardakhti
            // 
            this.txtBimar_NobatLab_Pardakhti.Location = new System.Drawing.Point(46, 76);
            this.txtBimar_NobatLab_Pardakhti.Name = "txtBimar_NobatLab_Pardakhti";
            this.txtBimar_NobatLab_Pardakhti.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatLab_Pardakhti.TabIndex = 66;
            // 
            // txtlblBimar_NobatLab_ShomarePaziresh
            // 
            this.txtlblBimar_NobatLab_ShomarePaziresh.AutoSize = true;
            this.txtlblBimar_NobatLab_ShomarePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_ShomarePaziresh.Location = new System.Drawing.Point(290, 99);
            this.txtlblBimar_NobatLab_ShomarePaziresh.Name = "txtlblBimar_NobatLab_ShomarePaziresh";
            this.txtlblBimar_NobatLab_ShomarePaziresh.Size = new System.Drawing.Size(30, 17);
            this.txtlblBimar_NobatLab_ShomarePaziresh.TabIndex = 59;
            this.txtlblBimar_NobatLab_ShomarePaziresh.Text = "نوبت :";
            // 
            // txtBimar_NobatLab_ShomarePaziresh
            // 
            this.txtBimar_NobatLab_ShomarePaziresh.Location = new System.Drawing.Point(266, 119);
            this.txtBimar_NobatLab_ShomarePaziresh.Name = "txtBimar_NobatLab_ShomarePaziresh";
            this.txtBimar_NobatLab_ShomarePaziresh.Size = new System.Drawing.Size(51, 23);
            this.txtBimar_NobatLab_ShomarePaziresh.TabIndex = 60;
            // 
            // txtlblBimar_NobatLab_Pardakhti
            // 
            this.txtlblBimar_NobatLab_Pardakhti.AutoSize = true;
            this.txtlblBimar_NobatLab_Pardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Pardakhti.Location = new System.Drawing.Point(191, 55);
            this.txtlblBimar_NobatLab_Pardakhti.Name = "txtlblBimar_NobatLab_Pardakhti";
            this.txtlblBimar_NobatLab_Pardakhti.Size = new System.Drawing.Size(45, 17);
            this.txtlblBimar_NobatLab_Pardakhti.TabIndex = 65;
            this.txtlblBimar_NobatLab_Pardakhti.Text = "پرداختی :";
            // 
            // dateBimar_NobatLab_DatePaziresh
            // 
            this.dateBimar_NobatLab_DatePaziresh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateBimar_NobatLab_DatePaziresh.Location = new System.Drawing.Point(336, 119);
            this.dateBimar_NobatLab_DatePaziresh.Name = "dateBimar_NobatLab_DatePaziresh";
            this.dateBimar_NobatLab_DatePaziresh.Size = new System.Drawing.Size(115, 23);
            this.dateBimar_NobatLab_DatePaziresh.TabIndex = 58;
            // 
            // txtBimar_NobatLab_Mablagh
            // 
            this.txtBimar_NobatLab_Mablagh.Location = new System.Drawing.Point(46, 30);
            this.txtBimar_NobatLab_Mablagh.Name = "txtBimar_NobatLab_Mablagh";
            this.txtBimar_NobatLab_Mablagh.Size = new System.Drawing.Size(135, 23);
            this.txtBimar_NobatLab_Mablagh.TabIndex = 61;
            // 
            // txtlblBimar_NobatLab_MandePardakhti
            // 
            this.txtlblBimar_NobatLab_MandePardakhti.AutoSize = true;
            this.txtlblBimar_NobatLab_MandePardakhti.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_MandePardakhti.Location = new System.Drawing.Point(170, 102);
            this.txtlblBimar_NobatLab_MandePardakhti.Name = "txtlblBimar_NobatLab_MandePardakhti";
            this.txtlblBimar_NobatLab_MandePardakhti.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatLab_MandePardakhti.TabIndex = 64;
            this.txtlblBimar_NobatLab_MandePardakhti.Text = "مانده پرداختی :";
            // 
            // txtlblBimar_NobatLab_DatePaziresh
            // 
            this.txtlblBimar_NobatLab_DatePaziresh.AutoSize = true;
            this.txtlblBimar_NobatLab_DatePaziresh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_DatePaziresh.Location = new System.Drawing.Point(423, 99);
            this.txtlblBimar_NobatLab_DatePaziresh.Name = "txtlblBimar_NobatLab_DatePaziresh";
            this.txtlblBimar_NobatLab_DatePaziresh.Size = new System.Drawing.Size(61, 17);
            this.txtlblBimar_NobatLab_DatePaziresh.TabIndex = 57;
            this.txtlblBimar_NobatLab_DatePaziresh.Text = "تاریخ پذیرش :";
            // 
            // txtlblBimar_NobatLab_ChooseDoc
            // 
            this.txtlblBimar_NobatLab_ChooseDoc.AutoSize = true;
            this.txtlblBimar_NobatLab_ChooseDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_ChooseDoc.Location = new System.Drawing.Point(418, 53);
            this.txtlblBimar_NobatLab_ChooseDoc.Name = "txtlblBimar_NobatLab_ChooseDoc";
            this.txtlblBimar_NobatLab_ChooseDoc.Size = new System.Drawing.Size(66, 17);
            this.txtlblBimar_NobatLab_ChooseDoc.TabIndex = 55;
            this.txtlblBimar_NobatLab_ChooseDoc.Text = "انتخاب پزشک :";
            // 
            // txtlblBimar_NobatLab_Mablagh
            // 
            this.txtlblBimar_NobatLab_Mablagh.AutoSize = true;
            this.txtlblBimar_NobatLab_Mablagh.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblBimar_NobatLab_Mablagh.Location = new System.Drawing.Point(177, 10);
            this.txtlblBimar_NobatLab_Mablagh.Name = "txtlblBimar_NobatLab_Mablagh";
            this.txtlblBimar_NobatLab_Mablagh.Size = new System.Drawing.Size(59, 17);
            this.txtlblBimar_NobatLab_Mablagh.TabIndex = 62;
            this.txtlblBimar_NobatLab_Mablagh.Text = "قابل پرداخت :";
            // 
            // txtBimar_NobatLab_ChooseDoc
            // 
            this.txtBimar_NobatLab_ChooseDoc.FormattingEnabled = true;
            this.txtBimar_NobatLab_ChooseDoc.Location = new System.Drawing.Point(266, 71);
            this.txtBimar_NobatLab_ChooseDoc.Name = "txtBimar_NobatLab_ChooseDoc";
            this.txtBimar_NobatLab_ChooseDoc.Size = new System.Drawing.Size(185, 25);
            this.txtBimar_NobatLab_ChooseDoc.TabIndex = 56;
            // 
            // ImgTabBimar
            // 
            this.ImgTabBimar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgTabBimar.ImageStream")));
            this.ImgTabBimar.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgTabBimar.Images.SetKeyName(0, "tbBimar_NobatPezeshk.png");
            this.ImgTabBimar.Images.SetKeyName(1, "tbBimar_NobatTasvirbardari.png");
            this.ImgTabBimar.Images.SetKeyName(2, "tbBimar_NobatKhadamat.png");
            this.ImgTabBimar.Images.SetKeyName(3, "tbBimar_NobatAzmayeshgah.png");
            // 
            // dgvBimar
            // 
            this.dgvBimar.AllowUserToAddRows = false;
            this.dgvBimar.AllowUserToDeleteRows = false;
            this.dgvBimar.AutoGenerateColumns = false;
            this.dgvBimar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBimar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codeMeliDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.bimeDataGridViewTextBoxColumn,
            this.chooseDocDataGridViewTextBoxColumn,
            this.datePazireshDataGridViewTextBoxColumn,
            this.ShomarePaziresh,
            this.Mablagh,
            this.Pardakhti,
            this.MandePardakhti});
            this.dgvBimar.DataSource = this.dbsBimar;
            this.dgvBimar.GridColor = System.Drawing.SystemColors.Control;
            this.dgvBimar.Location = new System.Drawing.Point(5, 243);
            this.dgvBimar.Name = "dgvBimar";
            this.dgvBimar.ReadOnly = true;
            this.dgvBimar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvBimar.Size = new System.Drawing.Size(763, 190);
            this.dgvBimar.TabIndex = 1;
            // 
            // dbsBimar
            // 
            this.dbsBimar.DataSource = typeof(Darmangah.DataModel.dmBimar);
            // 
            // btnfrmBimar_Print
            // 
            this.btnfrmBimar_Print.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmBimar_Print.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmBimar_Print.Image")));
            this.btnfrmBimar_Print.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmBimar_Print.Location = new System.Drawing.Point(524, 439);
            this.btnfrmBimar_Print.Name = "btnfrmBimar_Print";
            this.btnfrmBimar_Print.Size = new System.Drawing.Size(150, 30);
            this.btnfrmBimar_Print.TabIndex = 2;
            this.btnfrmBimar_Print.Text = "پرینت";
            this.btnfrmBimar_Print.UseVisualStyleBackColor = true;
            // 
            // btnfrmBimar_CancleNobat
            // 
            this.btnfrmBimar_CancleNobat.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmBimar_CancleNobat.Location = new System.Drawing.Point(350, 439);
            this.btnfrmBimar_CancleNobat.Name = "btnfrmBimar_CancleNobat";
            this.btnfrmBimar_CancleNobat.Size = new System.Drawing.Size(150, 30);
            this.btnfrmBimar_CancleNobat.TabIndex = 3;
            this.btnfrmBimar_CancleNobat.Text = "لغو نوبت";
            this.btnfrmBimar_CancleNobat.UseVisualStyleBackColor = true;
            // 
            // btnfrmBimar_EditNobat
            // 
            this.btnfrmBimar_EditNobat.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmBimar_EditNobat.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmBimar_EditNobat.Image")));
            this.btnfrmBimar_EditNobat.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmBimar_EditNobat.Location = new System.Drawing.Point(173, 439);
            this.btnfrmBimar_EditNobat.Name = "btnfrmBimar_EditNobat";
            this.btnfrmBimar_EditNobat.Size = new System.Drawing.Size(150, 30);
            this.btnfrmBimar_EditNobat.TabIndex = 4;
            this.btnfrmBimar_EditNobat.Text = "اصلاح اطلاعات";
            this.btnfrmBimar_EditNobat.UseVisualStyleBackColor = true;
            // 
            // btnfrmBimar_Search_SearchNobat
            // 
            this.btnfrmBimar_Search_SearchNobat.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmBimar_Search_SearchNobat.Image = ((System.Drawing.Image)(resources.GetObject("btnfrmBimar_Search_SearchNobat.Image")));
            this.btnfrmBimar_Search_SearchNobat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmBimar_Search_SearchNobat.Location = new System.Drawing.Point(52, 43);
            this.btnfrmBimar_Search_SearchNobat.Name = "btnfrmBimar_Search_SearchNobat";
            this.btnfrmBimar_Search_SearchNobat.Size = new System.Drawing.Size(153, 33);
            this.btnfrmBimar_Search_SearchNobat.TabIndex = 5;
            this.btnfrmBimar_Search_SearchNobat.Text = "جستجوی نوبت";
            this.btnfrmBimar_Search_SearchNobat.UseVisualStyleBackColor = true;
            // 
            // txtfrmBimar_Search_Kodemelli
            // 
            this.txtfrmBimar_Search_Kodemelli.Location = new System.Drawing.Point(451, 17);
            this.txtfrmBimar_Search_Kodemelli.Name = "txtfrmBimar_Search_Kodemelli";
            this.txtfrmBimar_Search_Kodemelli.Size = new System.Drawing.Size(140, 28);
            this.txtfrmBimar_Search_Kodemelli.TabIndex = 6;
            // 
            // datefrmBimar_Search_From
            // 
            this.datefrmBimar_Search_From.CalendarFont = new System.Drawing.Font("B Nazanin", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.datefrmBimar_Search_From.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datefrmBimar_Search_From.Location = new System.Drawing.Point(592, 53);
            this.datefrmBimar_Search_From.Name = "datefrmBimar_Search_From";
            this.datefrmBimar_Search_From.Size = new System.Drawing.Size(136, 28);
            this.datefrmBimar_Search_From.TabIndex = 7;
            // 
            // datefrmBimar_Search_To
            // 
            this.datefrmBimar_Search_To.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datefrmBimar_Search_To.Location = new System.Drawing.Point(317, 51);
            this.datefrmBimar_Search_To.Name = "datefrmBimar_Search_To";
            this.datefrmBimar_Search_To.Size = new System.Drawing.Size(136, 28);
            this.datefrmBimar_Search_To.TabIndex = 8;
            // 
            // grpfrmBimar_Search
            // 
            this.grpfrmBimar_Search.BackColor = System.Drawing.Color.Transparent;
            this.grpfrmBimar_Search.Controls.Add(this.label2);
            this.grpfrmBimar_Search.Controls.Add(this.label1);
            this.grpfrmBimar_Search.Controls.Add(this.txtlblBimar_Search_Kodemelli);
            this.grpfrmBimar_Search.Controls.Add(this.btnfrmBimar_Search_SearchNobat);
            this.grpfrmBimar_Search.Controls.Add(this.datefrmBimar_Search_To);
            this.grpfrmBimar_Search.Controls.Add(this.datefrmBimar_Search_From);
            this.grpfrmBimar_Search.Controls.Add(this.txtfrmBimar_Search_Kodemelli);
            this.grpfrmBimar_Search.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grpfrmBimar_Search.Location = new System.Drawing.Point(8, 473);
            this.grpfrmBimar_Search.Name = "grpfrmBimar_Search";
            this.grpfrmBimar_Search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpfrmBimar_Search.Size = new System.Drawing.Size(764, 86);
            this.grpfrmBimar_Search.TabIndex = 9;
            this.grpfrmBimar_Search.TabStop = false;
            this.grpfrmBimar_Search.Text = "جستجوی نوبت";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(459, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "تا";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(734, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "از";
            // 
            // txtlblBimar_Search_Kodemelli
            // 
            this.txtlblBimar_Search_Kodemelli.AutoSize = true;
            this.txtlblBimar_Search_Kodemelli.Location = new System.Drawing.Point(597, 20);
            this.txtlblBimar_Search_Kodemelli.Name = "txtlblBimar_Search_Kodemelli";
            this.txtlblBimar_Search_Kodemelli.Size = new System.Drawing.Size(69, 20);
            this.txtlblBimar_Search_Kodemelli.TabIndex = 9;
            this.txtlblBimar_Search_Kodemelli.Text = "کدملی بیمار :";
            // 
            // codeMeliDataGridViewTextBoxColumn
            // 
            this.codeMeliDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.codeMeliDataGridViewTextBoxColumn.DataPropertyName = "codeMeli";
            this.codeMeliDataGridViewTextBoxColumn.FillWeight = 528.1554F;
            this.codeMeliDataGridViewTextBoxColumn.HeaderText = "کدملی";
            this.codeMeliDataGridViewTextBoxColumn.Name = "codeMeliDataGridViewTextBoxColumn";
            this.codeMeliDataGridViewTextBoxColumn.ReadOnly = true;
            this.codeMeliDataGridViewTextBoxColumn.Width = 150;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.FillWeight = 38.83495F;
            this.nameDataGridViewTextBoxColumn.HeaderText = "نام";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bimeDataGridViewTextBoxColumn
            // 
            this.bimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.bimeDataGridViewTextBoxColumn.DataPropertyName = "Bime";
            this.bimeDataGridViewTextBoxColumn.FillWeight = 38.83495F;
            this.bimeDataGridViewTextBoxColumn.HeaderText = "بیمه";
            this.bimeDataGridViewTextBoxColumn.Name = "bimeDataGridViewTextBoxColumn";
            this.bimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // chooseDocDataGridViewTextBoxColumn
            // 
            this.chooseDocDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.chooseDocDataGridViewTextBoxColumn.DataPropertyName = "ChooseDoc";
            this.chooseDocDataGridViewTextBoxColumn.FillWeight = 38.83495F;
            this.chooseDocDataGridViewTextBoxColumn.HeaderText = "پزشک";
            this.chooseDocDataGridViewTextBoxColumn.Name = "chooseDocDataGridViewTextBoxColumn";
            this.chooseDocDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // datePazireshDataGridViewTextBoxColumn
            // 
            this.datePazireshDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.datePazireshDataGridViewTextBoxColumn.DataPropertyName = "DatePaziresh";
            this.datePazireshDataGridViewTextBoxColumn.FillWeight = 38.83495F;
            this.datePazireshDataGridViewTextBoxColumn.HeaderText = "روز";
            this.datePazireshDataGridViewTextBoxColumn.Name = "datePazireshDataGridViewTextBoxColumn";
            this.datePazireshDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ShomarePaziresh
            // 
            this.ShomarePaziresh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ShomarePaziresh.DataPropertyName = "ShomarePaziresh";
            this.ShomarePaziresh.HeaderText = "نوبت";
            this.ShomarePaziresh.Name = "ShomarePaziresh";
            this.ShomarePaziresh.ReadOnly = true;
            this.ShomarePaziresh.Width = 54;
            // 
            // Mablagh
            // 
            this.Mablagh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mablagh.DataPropertyName = "Mablagh";
            this.Mablagh.FillWeight = 38.83495F;
            this.Mablagh.HeaderText = "مبلغ";
            this.Mablagh.Name = "Mablagh";
            this.Mablagh.ReadOnly = true;
            // 
            // Pardakhti
            // 
            this.Pardakhti.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Pardakhti.DataPropertyName = "Pardakhti";
            this.Pardakhti.FillWeight = 38.83495F;
            this.Pardakhti.HeaderText = "پرداختی";
            this.Pardakhti.Name = "Pardakhti";
            this.Pardakhti.ReadOnly = true;
            // 
            // MandePardakhti
            // 
            this.MandePardakhti.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MandePardakhti.DataPropertyName = "MandePardakhti";
            this.MandePardakhti.FillWeight = 38.83495F;
            this.MandePardakhti.HeaderText = "مانده";
            this.MandePardakhti.Name = "MandePardakhti";
            this.MandePardakhti.ReadOnly = true;
            // 
            // frmBimar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.grpfrmBimar_Search);
            this.Controls.Add(this.btnfrmBimar_EditNobat);
            this.Controls.Add(this.btnfrmBimar_CancleNobat);
            this.Controls.Add(this.btnfrmBimar_Print);
            this.Controls.Add(this.dgvBimar);
            this.Controls.Add(this.tabfrmBimar_Nobat);
            this.Name = "frmBimar";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "نوبت دهی بیماران";
            this.tabfrmBimar_Nobat.ResumeLayout(false);
            this.tbBimar_NobatDoc.ResumeLayout(false);
            this.tbBimar_NobatDoc.PerformLayout();
            this.tbBimar_NobatKhadamat.ResumeLayout(false);
            this.tbBimar_NobatKhadamat.PerformLayout();
            this.tbBimar_NobatScan.ResumeLayout(false);
            this.tbBimar_NobatScan.PerformLayout();
            this.tbBimar_NobatLab.ResumeLayout(false);
            this.tbBimar_NobatLab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBimar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsBimar)).EndInit();
            this.grpfrmBimar_Search.ResumeLayout(false);
            this.grpfrmBimar_Search.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabfrmBimar_Nobat;
        private System.Windows.Forms.TabPage tbBimar_NobatDoc;
        private System.Windows.Forms.TabPage tbBimar_NobatKhadamat;
        private System.Windows.Forms.TabPage tbBimar_NobatScan;
        private System.Windows.Forms.TabPage tbBimar_NobatLab;
        private System.Windows.Forms.DataGridView dgvBimar;
        private System.Windows.Forms.Button btnfrmBimar_Print;
        private System.Windows.Forms.Button btnfrmBimar_CancleNobat;
        private System.Windows.Forms.Button btnfrmBimar_EditNobat;
        private System.Windows.Forms.Button btnfrmBimar_Search_SearchNobat;
        private System.Windows.Forms.TextBox txtfrmBimar_Search_Kodemelli;
        private System.Windows.Forms.DateTimePicker datefrmBimar_Search_From;
        private System.Windows.Forms.DateTimePicker datefrmBimar_Search_To;
        private System.Windows.Forms.GroupBox grpfrmBimar_Search;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txtlblBimar_Search_Kodemelli;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_ChooseKhedmat;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_KodeMelli;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_KodeMelli;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_Bime;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_Bime;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_Name;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_Name;
        private System.Windows.Forms.ComboBox cmbBimar_NobatDoc_ChooseKhedmat;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_KodeBime;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_KodeBime;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_Mobile;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_Mobile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnBimar_NobatDoc_Print;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBimar_NobatDoc_Save;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_MandePardakhti;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_Pardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_ShomarePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_ShomarePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_Pardakhti;
        private System.Windows.Forms.DateTimePicker dateBimar_NobatDoc_DatePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatDoc_Mablagh;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_MandePardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_DatePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatDoc_Mablagh;
        private System.Windows.Forms.ComboBox cmbBimar_NobatDoc_ChooseDoc;
        private System.Windows.Forms.ImageList ImgTabBimar;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_KodeBime;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Mobile;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Mobile;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_KodeBime;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbBimar_NobatKhadamat_ChooseKhedmat;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_ChooseKhedmat;
        private System.Windows.Forms.Button btnBimar_NobatKhadamat_Print;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Kodemelli;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Kodemelli;
        private System.Windows.Forms.Button btnBimar_NobatKhadamat_Save;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Bime;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Bime;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Name;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Name;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_MandePardakhti;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Pardakhti;
        private System.Windows.Forms.Label label18txtlblBimar_NobatKhadamat_ShomarePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_ShomarePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Pardakhti;
        private System.Windows.Forms.DateTimePicker dateBimar_NobatKhadamat_DatePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatKhadamat_Mablagh;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_MandePardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_DatePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatKhadamat_Mablagh;
        private System.Windows.Forms.ComboBox cmbBimar_NobatKhadamat_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_KodeBime;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Mobile;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Mobile;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_KodeBime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbBimar_NobatScan_ChooseKhedmat;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_ChooseKhedmat;
        private System.Windows.Forms.Button btnBimar_NobatScan_Print;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Kodemelli;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Kodemelli;
        private System.Windows.Forms.Button btnBimar_NobatScan_Save;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Bime;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Bime;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Name;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Name;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_MandePardakhti;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Pardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_ShomarePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_ShomarePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Pardakhti;
        private System.Windows.Forms.DateTimePicker dateBimar_NobatScan_DatePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatScan_Mablagh;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_MandePardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_DatePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatScan_Mablagh;
        private System.Windows.Forms.ComboBox cmbBimar_NobatScan_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_KodeBime;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Mobile;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Mobile;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_KodeBime;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbBimar_NobatLab_ChooseKhedmat;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_ChooseKhedmat;
        private System.Windows.Forms.Button btnBimar_NobatLab_Print;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Kodemelli;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Kodemelli;
        private System.Windows.Forms.Button btnBimar_NobatLab_Save;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Bime;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Bime;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Name;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Name;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_MandePardakhti;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Pardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_ShomarePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_ShomarePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Pardakhti;
        private System.Windows.Forms.DateTimePicker dateBimar_NobatLab_DatePaziresh;
        private System.Windows.Forms.TextBox txtBimar_NobatLab_Mablagh;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_MandePardakhti;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_DatePaziresh;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_ChooseDoc;
        private System.Windows.Forms.Label txtlblBimar_NobatLab_Mablagh;
        private System.Windows.Forms.ComboBox txtBimar_NobatLab_ChooseDoc;
        private System.Windows.Forms.BindingSource dbsBimar;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeMeliDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn chooseDocDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePazireshDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShomarePaziresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mablagh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pardakhti;
        private System.Windows.Forms.DataGridViewTextBoxColumn MandePardakhti;
    }
}