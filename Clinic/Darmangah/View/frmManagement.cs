﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Darmangah.View
{
    public partial class frmManagement : Form
    {
        public frmManagement()
        {
            InitializeComponent();
        }

        private void tabfrmManage_Choose_DrawItem(object sender, DrawItemEventArgs e)
        {
            var g = e.Graphics;
            var text = this.tabfrmManage_Choose.TabPages[e.Index].Text;
            var sizeText = g.MeasureString(text, this.tabfrmManage_Choose.Font);

            var x = e.Bounds.Left + 3;
            var y = e.Bounds.Top + (e.Bounds.Height - sizeText.Height) / 2;
            g.DrawString(text, this.tabfrmManage_Choose.Font, Brushes.Black, x, y);
        }


    }
}
