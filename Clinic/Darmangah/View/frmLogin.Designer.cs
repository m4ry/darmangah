﻿namespace Darmangah
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.txtlblfrmLogin_Titr = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtfrmLogin_User = new System.Windows.Forms.TextBox();
            this.btnfrmLogin_Vorood = new System.Windows.Forms.Button();
            this.cmbfrmLogin_User = new System.Windows.Forms.ComboBox();
            this.btnfrmLogin_Khorooj = new System.Windows.Forms.Button();
            this.txtLogin_Hadis = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtlblfrmLogin_Titr
            // 
            this.txtlblfrmLogin_Titr.AutoSize = true;
            this.txtlblfrmLogin_Titr.BackColor = System.Drawing.Color.Transparent;
            this.txtlblfrmLogin_Titr.Font = new System.Drawing.Font("B Nazanin", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmLogin_Titr.ForeColor = System.Drawing.Color.DimGray;
            this.txtlblfrmLogin_Titr.Location = new System.Drawing.Point(305, 25);
            this.txtlblfrmLogin_Titr.Name = "txtlblfrmLogin_Titr";
            this.txtlblfrmLogin_Titr.Size = new System.Drawing.Size(191, 35);
            this.txtlblfrmLogin_Titr.TabIndex = 0;
            this.txtlblfrmLogin_Titr.Text = "درمانگاه خیریه دوستان";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(643, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "نام کاربری";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(650, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "رمز عبور";
            // 
            // txtfrmLogin_User
            // 
            this.txtfrmLogin_User.Location = new System.Drawing.Point(500, 264);
            this.txtfrmLogin_User.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtfrmLogin_User.Name = "txtfrmLogin_User";
            this.txtfrmLogin_User.Size = new System.Drawing.Size(121, 23);
            this.txtfrmLogin_User.TabIndex = 4;
            // 
            // btnfrmLogin_Vorood
            // 
            this.btnfrmLogin_Vorood.Font = new System.Drawing.Font("B Nazanin", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmLogin_Vorood.Image = global::Darmangah.ImageResource.btnfrmLogin_Vorood;
            this.btnfrmLogin_Vorood.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmLogin_Vorood.Location = new System.Drawing.Point(504, 334);
            this.btnfrmLogin_Vorood.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnfrmLogin_Vorood.Name = "btnfrmLogin_Vorood";
            this.btnfrmLogin_Vorood.Size = new System.Drawing.Size(117, 50);
            this.btnfrmLogin_Vorood.TabIndex = 5;
            this.btnfrmLogin_Vorood.Text = "ورود           ";
            this.btnfrmLogin_Vorood.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmLogin_Vorood.UseVisualStyleBackColor = true;
            this.btnfrmLogin_Vorood.Click += new System.EventHandler(this.btnfrmLogin_Vorood_Click);
            this.btnfrmLogin_Vorood.MouseLeave += new System.EventHandler(this.btnfrmLogin_Vorood_MouseLeave);
            this.btnfrmLogin_Vorood.MouseHover += new System.EventHandler(this.btnfrmLogin_Vorood_MouseHover);
            // 
            // cmbfrmLogin_User
            // 
            this.cmbfrmLogin_User.FormattingEnabled = true;
            this.cmbfrmLogin_User.Location = new System.Drawing.Point(500, 210);
            this.cmbfrmLogin_User.Name = "cmbfrmLogin_User";
            this.cmbfrmLogin_User.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbfrmLogin_User.Size = new System.Drawing.Size(121, 25);
            this.cmbfrmLogin_User.TabIndex = 6;
            // 
            // btnfrmLogin_Khorooj
            // 
            this.btnfrmLogin_Khorooj.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmLogin_Khorooj.Image = global::Darmangah.ImageResource.btnfrmLogin_Khorooj;
            this.btnfrmLogin_Khorooj.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfrmLogin_Khorooj.Location = new System.Drawing.Point(504, 422);
            this.btnfrmLogin_Khorooj.Name = "btnfrmLogin_Khorooj";
            this.btnfrmLogin_Khorooj.Size = new System.Drawing.Size(117, 50);
            this.btnfrmLogin_Khorooj.TabIndex = 7;
            this.btnfrmLogin_Khorooj.Text = "انصراف";
            this.btnfrmLogin_Khorooj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfrmLogin_Khorooj.UseVisualStyleBackColor = true;
            // 
            // txtLogin_Hadis
            // 
            this.txtLogin_Hadis.BackColor = System.Drawing.Color.LightBlue;
            this.txtLogin_Hadis.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtLogin_Hadis.Location = new System.Drawing.Point(101, 86);
            this.txtLogin_Hadis.Multiline = true;
            this.txtLogin_Hadis.Name = "txtLogin_Hadis";
            this.txtLogin_Hadis.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLogin_Hadis.Size = new System.Drawing.Size(601, 76);
            this.txtLogin_Hadis.TabIndex = 9;
            this.txtLogin_Hadis.Text = resources.GetString("txtLogin_Hadis.Text");
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Darmangah.ImageResource.frmLogin;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(782, 553);
            this.Controls.Add(this.txtLogin_Hadis);
            this.Controls.Add(this.btnfrmLogin_Khorooj);
            this.Controls.Add(this.cmbfrmLogin_User);
            this.Controls.Add(this.btnfrmLogin_Vorood);
            this.Controls.Add(this.txtfrmLogin_User);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtlblfrmLogin_Titr);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "پنل ورود کاربر ";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtlblfrmLogin_Titr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtfrmLogin_User;
        private System.Windows.Forms.Button btnfrmLogin_Vorood;
        private System.Windows.Forms.ComboBox cmbfrmLogin_User;
        private System.Windows.Forms.Button btnfrmLogin_Khorooj;
        private System.Windows.Forms.TextBox txtLogin_Hadis;
    }
}

