﻿namespace Darmangah.View
{
    partial class frmChangePass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbfrmChangePass_Name = new System.Windows.Forms.ComboBox();
            this.txtfrmChangePass_NewPass = new System.Windows.Forms.TextBox();
            this.txtlblfrmChangePass_NewPass = new System.Windows.Forms.Label();
            this.txtlblfrmChangePass_Name = new System.Windows.Forms.Label();
            this.txtfrmChangePass_RepeatPass = new System.Windows.Forms.TextBox();
            this.txtlblfrmChangePass_RepeatPass = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbfrmChangePass_Name
            // 
            this.cmbfrmChangePass_Name.FormattingEnabled = true;
            this.cmbfrmChangePass_Name.Location = new System.Drawing.Point(331, 138);
            this.cmbfrmChangePass_Name.Name = "cmbfrmChangePass_Name";
            this.cmbfrmChangePass_Name.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbfrmChangePass_Name.Size = new System.Drawing.Size(121, 21);
            this.cmbfrmChangePass_Name.TabIndex = 10;
            // 
            // txtfrmChangePass_NewPass
            // 
            this.txtfrmChangePass_NewPass.Location = new System.Drawing.Point(331, 192);
            this.txtfrmChangePass_NewPass.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtfrmChangePass_NewPass.Name = "txtfrmChangePass_NewPass";
            this.txtfrmChangePass_NewPass.Size = new System.Drawing.Size(121, 20);
            this.txtfrmChangePass_NewPass.TabIndex = 9;
            // 
            // txtlblfrmChangePass_NewPass
            // 
            this.txtlblfrmChangePass_NewPass.AutoSize = true;
            this.txtlblfrmChangePass_NewPass.BackColor = System.Drawing.Color.Transparent;
            this.txtlblfrmChangePass_NewPass.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmChangePass_NewPass.Location = new System.Drawing.Point(481, 189);
            this.txtlblfrmChangePass_NewPass.Name = "txtlblfrmChangePass_NewPass";
            this.txtlblfrmChangePass_NewPass.Size = new System.Drawing.Size(82, 23);
            this.txtlblfrmChangePass_NewPass.TabIndex = 8;
            this.txtlblfrmChangePass_NewPass.Text = "رمز عبور جدید";
            // 
            // txtlblfrmChangePass_Name
            // 
            this.txtlblfrmChangePass_Name.AutoSize = true;
            this.txtlblfrmChangePass_Name.BackColor = System.Drawing.Color.Transparent;
            this.txtlblfrmChangePass_Name.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmChangePass_Name.Location = new System.Drawing.Point(474, 136);
            this.txtlblfrmChangePass_Name.Name = "txtlblfrmChangePass_Name";
            this.txtlblfrmChangePass_Name.Size = new System.Drawing.Size(59, 23);
            this.txtlblfrmChangePass_Name.TabIndex = 7;
            this.txtlblfrmChangePass_Name.Text = "نام کاربری";
            // 
            // txtfrmChangePass_RepeatPass
            // 
            this.txtfrmChangePass_RepeatPass.Location = new System.Drawing.Point(332, 238);
            this.txtfrmChangePass_RepeatPass.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtfrmChangePass_RepeatPass.Name = "txtfrmChangePass_RepeatPass";
            this.txtfrmChangePass_RepeatPass.Size = new System.Drawing.Size(121, 20);
            this.txtfrmChangePass_RepeatPass.TabIndex = 12;
            // 
            // txtlblfrmChangePass_RepeatPass
            // 
            this.txtlblfrmChangePass_RepeatPass.AutoSize = true;
            this.txtlblfrmChangePass_RepeatPass.BackColor = System.Drawing.Color.Transparent;
            this.txtlblfrmChangePass_RepeatPass.Font = new System.Drawing.Font("B Nazanin", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmChangePass_RepeatPass.Location = new System.Drawing.Point(482, 235);
            this.txtlblfrmChangePass_RepeatPass.Name = "txtlblfrmChangePass_RepeatPass";
            this.txtlblfrmChangePass_RepeatPass.Size = new System.Drawing.Size(79, 23);
            this.txtlblfrmChangePass_RepeatPass.TabIndex = 11;
            this.txtlblfrmChangePass_RepeatPass.Text = "تکرار رمز عبور";
            // 
            // frmChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.txtfrmChangePass_RepeatPass);
            this.Controls.Add(this.txtlblfrmChangePass_RepeatPass);
            this.Controls.Add(this.cmbfrmChangePass_Name);
            this.Controls.Add(this.txtfrmChangePass_NewPass);
            this.Controls.Add(this.txtlblfrmChangePass_NewPass);
            this.Controls.Add(this.txtlblfrmChangePass_Name);
            this.Name = "frmChangePass";
            this.Text = "تغییر کلمه عبور";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbfrmChangePass_Name;
        private System.Windows.Forms.TextBox txtfrmChangePass_NewPass;
        private System.Windows.Forms.Label txtlblfrmChangePass_NewPass;
        private System.Windows.Forms.Label txtlblfrmChangePass_Name;
        private System.Windows.Forms.TextBox txtfrmChangePass_RepeatPass;
        private System.Windows.Forms.Label txtlblfrmChangePass_RepeatPass;
    }
}