﻿namespace Darmangah.View
{
    partial class frmManageDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpfrmManageDoc_Personal = new System.Windows.Forms.GroupBox();
            this.cmbfrmManageDoc_Personal_Gender = new System.Windows.Forms.ComboBox();
            this.txtlblfrmManageDoc_Personal_Gender = new System.Windows.Forms.Label();
            this.cmbfrmManageDoc_Personal_Tel = new System.Windows.Forms.ComboBox();
            this.cmbfrmManageDoc_Personal_Mobile = new System.Windows.Forms.ComboBox();
            this.txtlblfrmManageDoc_Personal_Mobile = new System.Windows.Forms.Label();
            this.txtfrmManageDoc_Personal_Adres = new System.Windows.Forms.TextBox();
            this.txtlblfrmManageDoc_Personal_Adres = new System.Windows.Forms.Label();
            this.txtlblfrmManageDoc_Personal_Tel = new System.Windows.Forms.Label();
            this.txtfrmManageDoc_Personal_Kodemelli = new System.Windows.Forms.TextBox();
            this.txtlblfrmManageDoc_Personal_Kodemelli = new System.Windows.Forms.Label();
            this.txtfrmManageDoc_Personal_Name = new System.Windows.Forms.TextBox();
            this.txtlblfrmManageDoc_Personal_Name = new System.Windows.Forms.Label();
            this.grpfrmManageDoc_Madrak = new System.Windows.Forms.GroupBox();
            this.cmbManageDoc_Madrak_ChooseTakhasos = new System.Windows.Forms.ComboBox();
            this.txtlblfrmManageDoc_Madrak_ChooseEdu = new System.Windows.Forms.Label();
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos = new System.Windows.Forms.ComboBox();
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos = new System.Windows.Forms.Label();
            this.txtfrmManageDoc_Madrak_NezamDoc = new System.Windows.Forms.TextBox();
            this.txtlblfrmManageDoc_Madrak_NezamDoc = new System.Windows.Forms.Label();
            this.grpfrmManageDoc_Shift = new System.Windows.Forms.GroupBox();
            this.datefrmManageDoc_Shift_ChooseDate = new System.Windows.Forms.DateTimePicker();
            this.txtlblfrmManageDoc_Shift_ChooseDate = new System.Windows.Forms.Label();
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc = new System.Windows.Forms.ComboBox();
            this.txtlblfrmManageDoc_Shift_ChooseZaman = new System.Windows.Forms.Label();
            this.btnfrmManageDoc_Save = new System.Windows.Forms.Button();
            this.btnfrmManageDoc_Edit = new System.Windows.Forms.Button();
            this.btnfrmManageDoc_Delete = new System.Windows.Forms.Button();
            this.dgvDoctor = new System.Windows.Forms.DataGridView();
            this.txtfrmManageDoc_Search_Kodemelli = new System.Windows.Forms.TextBox();
            this.txtlblfrmManageDoc_Search_Kodemelli = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dbsDoctor = new System.Windows.Forms.BindingSource(this.components);
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeMeliDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nezamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noeTakhassosDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shiftDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateHozoorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpfrmManageDoc_Personal.SuspendLayout();
            this.grpfrmManageDoc_Madrak.SuspendLayout();
            this.grpfrmManageDoc_Shift.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDoctor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsDoctor)).BeginInit();
            this.SuspendLayout();
            // 
            // grpfrmManageDoc_Personal
            // 
            this.grpfrmManageDoc_Personal.Controls.Add(this.cmbfrmManageDoc_Personal_Gender);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Gender);
            this.grpfrmManageDoc_Personal.Controls.Add(this.cmbfrmManageDoc_Personal_Tel);
            this.grpfrmManageDoc_Personal.Controls.Add(this.cmbfrmManageDoc_Personal_Mobile);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Mobile);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtfrmManageDoc_Personal_Adres);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Adres);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Tel);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtfrmManageDoc_Personal_Kodemelli);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Kodemelli);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtfrmManageDoc_Personal_Name);
            this.grpfrmManageDoc_Personal.Controls.Add(this.txtlblfrmManageDoc_Personal_Name);
            this.grpfrmManageDoc_Personal.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grpfrmManageDoc_Personal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grpfrmManageDoc_Personal.Location = new System.Drawing.Point(12, 12);
            this.grpfrmManageDoc_Personal.Name = "grpfrmManageDoc_Personal";
            this.grpfrmManageDoc_Personal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpfrmManageDoc_Personal.Size = new System.Drawing.Size(760, 132);
            this.grpfrmManageDoc_Personal.TabIndex = 0;
            this.grpfrmManageDoc_Personal.TabStop = false;
            this.grpfrmManageDoc_Personal.Text = "اطلاعات فردی";
            // 
            // cmbfrmManageDoc_Personal_Gender
            // 
            this.cmbfrmManageDoc_Personal_Gender.FormattingEnabled = true;
            this.cmbfrmManageDoc_Personal_Gender.Items.AddRange(new object[] {
            "زن",
            "مرد"});
            this.cmbfrmManageDoc_Personal_Gender.Location = new System.Drawing.Point(412, 44);
            this.cmbfrmManageDoc_Personal_Gender.Name = "cmbfrmManageDoc_Personal_Gender";
            this.cmbfrmManageDoc_Personal_Gender.Size = new System.Drawing.Size(71, 28);
            this.cmbfrmManageDoc_Personal_Gender.TabIndex = 22;
            // 
            // txtlblfrmManageDoc_Personal_Gender
            // 
            this.txtlblfrmManageDoc_Personal_Gender.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Gender.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Gender.Location = new System.Drawing.Point(480, 24);
            this.txtlblfrmManageDoc_Personal_Gender.Name = "txtlblfrmManageDoc_Personal_Gender";
            this.txtlblfrmManageDoc_Personal_Gender.Size = new System.Drawing.Size(43, 17);
            this.txtlblfrmManageDoc_Personal_Gender.TabIndex = 21;
            this.txtlblfrmManageDoc_Personal_Gender.Text = "جنسیت :";
            // 
            // cmbfrmManageDoc_Personal_Tel
            // 
            this.cmbfrmManageDoc_Personal_Tel.FormattingEnabled = true;
            this.cmbfrmManageDoc_Personal_Tel.Location = new System.Drawing.Point(221, 44);
            this.cmbfrmManageDoc_Personal_Tel.Name = "cmbfrmManageDoc_Personal_Tel";
            this.cmbfrmManageDoc_Personal_Tel.Size = new System.Drawing.Size(135, 28);
            this.cmbfrmManageDoc_Personal_Tel.TabIndex = 20;
            // 
            // cmbfrmManageDoc_Personal_Mobile
            // 
            this.cmbfrmManageDoc_Personal_Mobile.FormattingEnabled = true;
            this.cmbfrmManageDoc_Personal_Mobile.Location = new System.Drawing.Point(24, 44);
            this.cmbfrmManageDoc_Personal_Mobile.Name = "cmbfrmManageDoc_Personal_Mobile";
            this.cmbfrmManageDoc_Personal_Mobile.Size = new System.Drawing.Size(135, 28);
            this.cmbfrmManageDoc_Personal_Mobile.TabIndex = 19;
            // 
            // txtlblfrmManageDoc_Personal_Mobile
            // 
            this.txtlblfrmManageDoc_Personal_Mobile.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Mobile.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Mobile.Location = new System.Drawing.Point(145, 24);
            this.txtlblfrmManageDoc_Personal_Mobile.Name = "txtlblfrmManageDoc_Personal_Mobile";
            this.txtlblfrmManageDoc_Personal_Mobile.Size = new System.Drawing.Size(52, 17);
            this.txtlblfrmManageDoc_Personal_Mobile.TabIndex = 15;
            this.txtlblfrmManageDoc_Personal_Mobile.Text = "تلفن همراه :";
            // 
            // txtfrmManageDoc_Personal_Adres
            // 
            this.txtfrmManageDoc_Personal_Adres.Location = new System.Drawing.Point(61, 98);
            this.txtfrmManageDoc_Personal_Adres.Name = "txtfrmManageDoc_Personal_Adres";
            this.txtfrmManageDoc_Personal_Adres.Size = new System.Drawing.Size(422, 28);
            this.txtfrmManageDoc_Personal_Adres.TabIndex = 14;
            // 
            // txtlblfrmManageDoc_Personal_Adres
            // 
            this.txtlblfrmManageDoc_Personal_Adres.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Adres.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Adres.Location = new System.Drawing.Point(474, 75);
            this.txtlblfrmManageDoc_Personal_Adres.Name = "txtlblfrmManageDoc_Personal_Adres";
            this.txtlblfrmManageDoc_Personal_Adres.Size = new System.Drawing.Size(39, 17);
            this.txtlblfrmManageDoc_Personal_Adres.TabIndex = 13;
            this.txtlblfrmManageDoc_Personal_Adres.Text = "  آدرس :";
            // 
            // txtlblfrmManageDoc_Personal_Tel
            // 
            this.txtlblfrmManageDoc_Personal_Tel.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Tel.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Tel.Location = new System.Drawing.Point(341, 24);
            this.txtlblfrmManageDoc_Personal_Tel.Name = "txtlblfrmManageDoc_Personal_Tel";
            this.txtlblfrmManageDoc_Personal_Tel.Size = new System.Drawing.Size(51, 17);
            this.txtlblfrmManageDoc_Personal_Tel.TabIndex = 11;
            this.txtlblfrmManageDoc_Personal_Tel.Text = "تلفن منزل :";
            // 
            // txtfrmManageDoc_Personal_Kodemelli
            // 
            this.txtfrmManageDoc_Personal_Kodemelli.Location = new System.Drawing.Point(536, 98);
            this.txtfrmManageDoc_Personal_Kodemelli.Name = "txtfrmManageDoc_Personal_Kodemelli";
            this.txtfrmManageDoc_Personal_Kodemelli.Size = new System.Drawing.Size(184, 28);
            this.txtfrmManageDoc_Personal_Kodemelli.TabIndex = 10;
            // 
            // txtlblfrmManageDoc_Personal_Kodemelli
            // 
            this.txtlblfrmManageDoc_Personal_Kodemelli.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Kodemelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Kodemelli.Location = new System.Drawing.Point(711, 75);
            this.txtlblfrmManageDoc_Personal_Kodemelli.Name = "txtlblfrmManageDoc_Personal_Kodemelli";
            this.txtlblfrmManageDoc_Personal_Kodemelli.Size = new System.Drawing.Size(43, 17);
            this.txtlblfrmManageDoc_Personal_Kodemelli.TabIndex = 9;
            this.txtlblfrmManageDoc_Personal_Kodemelli.Text = "  کد ملی :";
            // 
            // txtfrmManageDoc_Personal_Name
            // 
            this.txtfrmManageDoc_Personal_Name.Location = new System.Drawing.Point(536, 44);
            this.txtfrmManageDoc_Personal_Name.Name = "txtfrmManageDoc_Personal_Name";
            this.txtfrmManageDoc_Personal_Name.Size = new System.Drawing.Size(185, 28);
            this.txtfrmManageDoc_Personal_Name.TabIndex = 7;
            // 
            // txtlblfrmManageDoc_Personal_Name
            // 
            this.txtlblfrmManageDoc_Personal_Name.AutoSize = true;
            this.txtlblfrmManageDoc_Personal_Name.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Personal_Name.Location = new System.Drawing.Point(657, 24);
            this.txtlblfrmManageDoc_Personal_Name.Name = "txtlblfrmManageDoc_Personal_Name";
            this.txtlblfrmManageDoc_Personal_Name.Size = new System.Drawing.Size(105, 17);
            this.txtlblfrmManageDoc_Personal_Name.TabIndex = 6;
            this.txtlblfrmManageDoc_Personal_Name.Text = "نام و نام خانوادگی پزشک :";
            // 
            // grpfrmManageDoc_Madrak
            // 
            this.grpfrmManageDoc_Madrak.Controls.Add(this.cmbManageDoc_Madrak_ChooseTakhasos);
            this.grpfrmManageDoc_Madrak.Controls.Add(this.txtlblfrmManageDoc_Madrak_ChooseEdu);
            this.grpfrmManageDoc_Madrak.Controls.Add(this.cmbfrmManageDoc_Madrak_ChooseTakhasos);
            this.grpfrmManageDoc_Madrak.Controls.Add(this.txtlblfrmManageDoc_Madrak_ChooseTakhasos);
            this.grpfrmManageDoc_Madrak.Controls.Add(this.txtfrmManageDoc_Madrak_NezamDoc);
            this.grpfrmManageDoc_Madrak.Controls.Add(this.txtlblfrmManageDoc_Madrak_NezamDoc);
            this.grpfrmManageDoc_Madrak.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grpfrmManageDoc_Madrak.Location = new System.Drawing.Point(12, 150);
            this.grpfrmManageDoc_Madrak.Name = "grpfrmManageDoc_Madrak";
            this.grpfrmManageDoc_Madrak.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpfrmManageDoc_Madrak.Size = new System.Drawing.Size(760, 84);
            this.grpfrmManageDoc_Madrak.TabIndex = 1;
            this.grpfrmManageDoc_Madrak.TabStop = false;
            this.grpfrmManageDoc_Madrak.Text = "اطلاعات پزشکی";
            // 
            // cmbManageDoc_Madrak_ChooseTakhasos
            // 
            this.cmbManageDoc_Madrak_ChooseTakhasos.FormattingEnabled = true;
            this.cmbManageDoc_Madrak_ChooseTakhasos.Items.AddRange(new object[] {
            "عمومی",
            "تخصص",
            "فوق تخصص",
            "کارشناس",
            "مشاور",
            "فلوشیپ",
            "دکترا"});
            this.cmbManageDoc_Madrak_ChooseTakhasos.Location = new System.Drawing.Point(60, 49);
            this.cmbManageDoc_Madrak_ChooseTakhasos.Name = "cmbManageDoc_Madrak_ChooseTakhasos";
            this.cmbManageDoc_Madrak_ChooseTakhasos.Size = new System.Drawing.Size(185, 28);
            this.cmbManageDoc_Madrak_ChooseTakhasos.TabIndex = 16;
            // 
            // txtlblfrmManageDoc_Madrak_ChooseEdu
            // 
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.AutoSize = true;
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.Location = new System.Drawing.Point(219, 29);
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.Name = "txtlblfrmManageDoc_Madrak_ChooseEdu";
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.Size = new System.Drawing.Size(52, 17);
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.TabIndex = 15;
            this.txtlblfrmManageDoc_Madrak_ChooseEdu.Text = "  تحصیلات :";
            // 
            // cmbfrmManageDoc_Madrak_ChooseTakhasos
            // 
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.FormattingEnabled = true;
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.Items.AddRange(new object[] {
            "تخصص",
            "فوق تخصص"});
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.Location = new System.Drawing.Point(298, 49);
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.Name = "cmbfrmManageDoc_Madrak_ChooseTakhasos";
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.Size = new System.Drawing.Size(185, 28);
            this.cmbfrmManageDoc_Madrak_ChooseTakhasos.TabIndex = 14;
            // 
            // txtlblfrmManageDoc_Madrak_ChooseTakhasos
            // 
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.AutoSize = true;
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.Location = new System.Drawing.Point(457, 29);
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.Name = "txtlblfrmManageDoc_Madrak_ChooseTakhasos";
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.Size = new System.Drawing.Size(59, 17);
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.TabIndex = 13;
            this.txtlblfrmManageDoc_Madrak_ChooseTakhasos.Text = "  نوع تخصص :";
            // 
            // txtfrmManageDoc_Madrak_NezamDoc
            // 
            this.txtfrmManageDoc_Madrak_NezamDoc.Location = new System.Drawing.Point(535, 49);
            this.txtfrmManageDoc_Madrak_NezamDoc.Name = "txtfrmManageDoc_Madrak_NezamDoc";
            this.txtfrmManageDoc_Madrak_NezamDoc.Size = new System.Drawing.Size(185, 28);
            this.txtfrmManageDoc_Madrak_NezamDoc.TabIndex = 12;
            // 
            // txtlblfrmManageDoc_Madrak_NezamDoc
            // 
            this.txtlblfrmManageDoc_Madrak_NezamDoc.AutoSize = true;
            this.txtlblfrmManageDoc_Madrak_NezamDoc.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Madrak_NezamDoc.Location = new System.Drawing.Point(669, 29);
            this.txtlblfrmManageDoc_Madrak_NezamDoc.Name = "txtlblfrmManageDoc_Madrak_NezamDoc";
            this.txtlblfrmManageDoc_Madrak_NezamDoc.Size = new System.Drawing.Size(84, 17);
            this.txtlblfrmManageDoc_Madrak_NezamDoc.TabIndex = 11;
            this.txtlblfrmManageDoc_Madrak_NezamDoc.Text = "شماره نظام پزشکی :";
            // 
            // grpfrmManageDoc_Shift
            // 
            this.grpfrmManageDoc_Shift.Controls.Add(this.datefrmManageDoc_Shift_ChooseDate);
            this.grpfrmManageDoc_Shift.Controls.Add(this.txtlblfrmManageDoc_Shift_ChooseDate);
            this.grpfrmManageDoc_Shift.Controls.Add(this.cmbtxtfrmManageDoc_Madrak_NezamDoc);
            this.grpfrmManageDoc_Shift.Controls.Add(this.txtlblfrmManageDoc_Shift_ChooseZaman);
            this.grpfrmManageDoc_Shift.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grpfrmManageDoc_Shift.Location = new System.Drawing.Point(12, 240);
            this.grpfrmManageDoc_Shift.Name = "grpfrmManageDoc_Shift";
            this.grpfrmManageDoc_Shift.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpfrmManageDoc_Shift.Size = new System.Drawing.Size(760, 86);
            this.grpfrmManageDoc_Shift.TabIndex = 2;
            this.grpfrmManageDoc_Shift.TabStop = false;
            this.grpfrmManageDoc_Shift.Text = "اطلاعات حضور در درمانگاه";
            // 
            // datefrmManageDoc_Shift_ChooseDate
            // 
            this.datefrmManageDoc_Shift_ChooseDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datefrmManageDoc_Shift_ChooseDate.Location = new System.Drawing.Point(298, 49);
            this.datefrmManageDoc_Shift_ChooseDate.Name = "datefrmManageDoc_Shift_ChooseDate";
            this.datefrmManageDoc_Shift_ChooseDate.Size = new System.Drawing.Size(185, 28);
            this.datefrmManageDoc_Shift_ChooseDate.TabIndex = 15;
            // 
            // txtlblfrmManageDoc_Shift_ChooseDate
            // 
            this.txtlblfrmManageDoc_Shift_ChooseDate.AutoSize = true;
            this.txtlblfrmManageDoc_Shift_ChooseDate.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Shift_ChooseDate.Location = new System.Drawing.Point(432, 29);
            this.txtlblfrmManageDoc_Shift_ChooseDate.Name = "txtlblfrmManageDoc_Shift_ChooseDate";
            this.txtlblfrmManageDoc_Shift_ChooseDate.Size = new System.Drawing.Size(60, 17);
            this.txtlblfrmManageDoc_Shift_ChooseDate.TabIndex = 14;
            this.txtlblfrmManageDoc_Shift_ChooseDate.Text = "انتخاب تاریخ :";
            // 
            // cmbtxtfrmManageDoc_Madrak_NezamDoc
            // 
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.FormattingEnabled = true;
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.Items.AddRange(new object[] {
            "صبح",
            "عصر"});
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.Location = new System.Drawing.Point(535, 49);
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.Name = "cmbtxtfrmManageDoc_Madrak_NezamDoc";
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.Size = new System.Drawing.Size(185, 28);
            this.cmbtxtfrmManageDoc_Madrak_NezamDoc.TabIndex = 13;
            // 
            // txtlblfrmManageDoc_Shift_ChooseZaman
            // 
            this.txtlblfrmManageDoc_Shift_ChooseZaman.AutoSize = true;
            this.txtlblfrmManageDoc_Shift_ChooseZaman.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Shift_ChooseZaman.Location = new System.Drawing.Point(669, 29);
            this.txtlblfrmManageDoc_Shift_ChooseZaman.Name = "txtlblfrmManageDoc_Shift_ChooseZaman";
            this.txtlblfrmManageDoc_Shift_ChooseZaman.Size = new System.Drawing.Size(84, 17);
            this.txtlblfrmManageDoc_Shift_ChooseZaman.TabIndex = 12;
            this.txtlblfrmManageDoc_Shift_ChooseZaman.Text = "انتخاب شیفت کاری :";
            // 
            // btnfrmManageDoc_Save
            // 
            this.btnfrmManageDoc_Save.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmManageDoc_Save.Location = new System.Drawing.Point(562, 517);
            this.btnfrmManageDoc_Save.Name = "btnfrmManageDoc_Save";
            this.btnfrmManageDoc_Save.Size = new System.Drawing.Size(179, 30);
            this.btnfrmManageDoc_Save.TabIndex = 3;
            this.btnfrmManageDoc_Save.Text = "ثبت";
            this.btnfrmManageDoc_Save.UseVisualStyleBackColor = true;
            // 
            // btnfrmManageDoc_Edit
            // 
            this.btnfrmManageDoc_Edit.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmManageDoc_Edit.Location = new System.Drawing.Point(312, 517);
            this.btnfrmManageDoc_Edit.Name = "btnfrmManageDoc_Edit";
            this.btnfrmManageDoc_Edit.Size = new System.Drawing.Size(179, 30);
            this.btnfrmManageDoc_Edit.TabIndex = 4;
            this.btnfrmManageDoc_Edit.Text = "ویرایش";
            this.btnfrmManageDoc_Edit.UseVisualStyleBackColor = true;
            // 
            // btnfrmManageDoc_Delete
            // 
            this.btnfrmManageDoc_Delete.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnfrmManageDoc_Delete.Location = new System.Drawing.Point(51, 519);
            this.btnfrmManageDoc_Delete.Name = "btnfrmManageDoc_Delete";
            this.btnfrmManageDoc_Delete.Size = new System.Drawing.Size(179, 30);
            this.btnfrmManageDoc_Delete.TabIndex = 5;
            this.btnfrmManageDoc_Delete.Text = "حذف";
            this.btnfrmManageDoc_Delete.UseVisualStyleBackColor = true;
            // 
            // dgvDoctor
            // 
            this.dgvDoctor.AllowUserToAddRows = false;
            this.dgvDoctor.AllowUserToDeleteRows = false;
            this.dgvDoctor.AutoGenerateColumns = false;
            this.dgvDoctor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDoctor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.codeMeliDataGridViewTextBoxColumn,
            this.mobileDataGridViewTextBoxColumn,
            this.nezamDataGridViewTextBoxColumn,
            this.noeTakhassosDataGridViewTextBoxColumn,
            this.shiftDataGridViewTextBoxColumn,
            this.dateHozoorDataGridViewTextBoxColumn});
            this.dgvDoctor.DataSource = this.dbsDoctor;
            this.dgvDoctor.Location = new System.Drawing.Point(12, 381);
            this.dgvDoctor.Name = "dgvDoctor";
            this.dgvDoctor.ReadOnly = true;
            this.dgvDoctor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvDoctor.Size = new System.Drawing.Size(760, 120);
            this.dgvDoctor.TabIndex = 6;
            // 
            // txtfrmManageDoc_Search_Kodemelli
            // 
            this.txtfrmManageDoc_Search_Kodemelli.Location = new System.Drawing.Point(547, 355);
            this.txtfrmManageDoc_Search_Kodemelli.Name = "txtfrmManageDoc_Search_Kodemelli";
            this.txtfrmManageDoc_Search_Kodemelli.Size = new System.Drawing.Size(184, 20);
            this.txtfrmManageDoc_Search_Kodemelli.TabIndex = 12;
            // 
            // txtlblfrmManageDoc_Search_Kodemelli
            // 
            this.txtlblfrmManageDoc_Search_Kodemelli.AutoSize = true;
            this.txtlblfrmManageDoc_Search_Kodemelli.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtlblfrmManageDoc_Search_Kodemelli.Location = new System.Drawing.Point(694, 329);
            this.txtlblfrmManageDoc_Search_Kodemelli.Name = "txtlblfrmManageDoc_Search_Kodemelli";
            this.txtlblfrmManageDoc_Search_Kodemelli.Size = new System.Drawing.Size(69, 17);
            this.txtlblfrmManageDoc_Search_Kodemelli.TabIndex = 11;
            this.txtlblfrmManageDoc_Search_Kodemelli.Text = " : کدملی پزشک ";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(316, 345);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 30);
            this.button1.TabIndex = 13;
            this.button1.Text = "جستجو";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dbsDoctor
            // 
            this.dbsDoctor.DataSource = typeof(Darmangah.DataModel.dmDoctor);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.nameDataGridViewTextBoxColumn.HeaderText = "نام";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codeMeliDataGridViewTextBoxColumn
            // 
            this.codeMeliDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.codeMeliDataGridViewTextBoxColumn.DataPropertyName = "CodeMeli";
            this.codeMeliDataGridViewTextBoxColumn.FillWeight = 558.3756F;
            this.codeMeliDataGridViewTextBoxColumn.HeaderText = "کدملی";
            this.codeMeliDataGridViewTextBoxColumn.Name = "codeMeliDataGridViewTextBoxColumn";
            this.codeMeliDataGridViewTextBoxColumn.ReadOnly = true;
            this.codeMeliDataGridViewTextBoxColumn.Width = 150;
            // 
            // mobileDataGridViewTextBoxColumn
            // 
            this.mobileDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mobileDataGridViewTextBoxColumn.DataPropertyName = "Mobile";
            this.mobileDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.mobileDataGridViewTextBoxColumn.HeaderText = "موبایل";
            this.mobileDataGridViewTextBoxColumn.Name = "mobileDataGridViewTextBoxColumn";
            this.mobileDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nezamDataGridViewTextBoxColumn
            // 
            this.nezamDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nezamDataGridViewTextBoxColumn.DataPropertyName = "Nezam";
            this.nezamDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.nezamDataGridViewTextBoxColumn.HeaderText = "نظام پزشکی";
            this.nezamDataGridViewTextBoxColumn.Name = "nezamDataGridViewTextBoxColumn";
            this.nezamDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // noeTakhassosDataGridViewTextBoxColumn
            // 
            this.noeTakhassosDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.noeTakhassosDataGridViewTextBoxColumn.DataPropertyName = "NoeTakhassos";
            this.noeTakhassosDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.noeTakhassosDataGridViewTextBoxColumn.HeaderText = "تخصص";
            this.noeTakhassosDataGridViewTextBoxColumn.Name = "noeTakhassosDataGridViewTextBoxColumn";
            this.noeTakhassosDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shiftDataGridViewTextBoxColumn
            // 
            this.shiftDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.shiftDataGridViewTextBoxColumn.DataPropertyName = "Shift";
            this.shiftDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.shiftDataGridViewTextBoxColumn.HeaderText = "شیفت";
            this.shiftDataGridViewTextBoxColumn.Name = "shiftDataGridViewTextBoxColumn";
            this.shiftDataGridViewTextBoxColumn.ReadOnly = true;
            this.shiftDataGridViewTextBoxColumn.Width = 58;
            // 
            // dateHozoorDataGridViewTextBoxColumn
            // 
            this.dateHozoorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dateHozoorDataGridViewTextBoxColumn.DataPropertyName = "DateHozoor";
            this.dateHozoorDataGridViewTextBoxColumn.FillWeight = 54.16244F;
            this.dateHozoorDataGridViewTextBoxColumn.HeaderText = "روز حضور";
            this.dateHozoorDataGridViewTextBoxColumn.Name = "dateHozoorDataGridViewTextBoxColumn";
            this.dateHozoorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // frmManageDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtfrmManageDoc_Search_Kodemelli);
            this.Controls.Add(this.txtlblfrmManageDoc_Search_Kodemelli);
            this.Controls.Add(this.dgvDoctor);
            this.Controls.Add(this.btnfrmManageDoc_Delete);
            this.Controls.Add(this.btnfrmManageDoc_Edit);
            this.Controls.Add(this.btnfrmManageDoc_Save);
            this.Controls.Add(this.grpfrmManageDoc_Shift);
            this.Controls.Add(this.grpfrmManageDoc_Madrak);
            this.Controls.Add(this.grpfrmManageDoc_Personal);
            this.Name = "frmManageDoc";
            this.Text = "مدیریت پزشک";
            this.grpfrmManageDoc_Personal.ResumeLayout(false);
            this.grpfrmManageDoc_Personal.PerformLayout();
            this.grpfrmManageDoc_Madrak.ResumeLayout(false);
            this.grpfrmManageDoc_Madrak.PerformLayout();
            this.grpfrmManageDoc_Shift.ResumeLayout(false);
            this.grpfrmManageDoc_Shift.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDoctor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsDoctor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpfrmManageDoc_Personal;
        private System.Windows.Forms.GroupBox grpfrmManageDoc_Madrak;
        private System.Windows.Forms.GroupBox grpfrmManageDoc_Shift;
        private System.Windows.Forms.ComboBox cmbfrmManageDoc_Personal_Tel;
        private System.Windows.Forms.ComboBox cmbfrmManageDoc_Personal_Mobile;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Mobile;
        private System.Windows.Forms.TextBox txtfrmManageDoc_Personal_Adres;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Adres;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Tel;
        private System.Windows.Forms.TextBox txtfrmManageDoc_Personal_Kodemelli;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Kodemelli;
        private System.Windows.Forms.TextBox txtfrmManageDoc_Personal_Name;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Name;
        private System.Windows.Forms.ComboBox cmbfrmManageDoc_Madrak_ChooseTakhasos;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Madrak_ChooseTakhasos;
        private System.Windows.Forms.TextBox txtfrmManageDoc_Madrak_NezamDoc;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Madrak_NezamDoc;
        private System.Windows.Forms.Button btnfrmManageDoc_Save;
        private System.Windows.Forms.ComboBox cmbfrmManageDoc_Personal_Gender;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Personal_Gender;
        private System.Windows.Forms.DateTimePicker datefrmManageDoc_Shift_ChooseDate;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Shift_ChooseDate;
        private System.Windows.Forms.ComboBox cmbtxtfrmManageDoc_Madrak_NezamDoc;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Shift_ChooseZaman;
        private System.Windows.Forms.Button btnfrmManageDoc_Edit;
        private System.Windows.Forms.Button btnfrmManageDoc_Delete;
        private System.Windows.Forms.DataGridView dgvDoctor;
        private System.Windows.Forms.ComboBox cmbManageDoc_Madrak_ChooseTakhasos;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Madrak_ChooseEdu;
        private System.Windows.Forms.TextBox txtfrmManageDoc_Search_Kodemelli;
        private System.Windows.Forms.Label txtlblfrmManageDoc_Search_Kodemelli;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource dbsDoctor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeMeliDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nezamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn noeTakhassosDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shiftDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateHozoorDataGridViewTextBoxColumn;
    }
}